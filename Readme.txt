Extra cores for ZX Spectrum Next

The sources comes from several open source projects and have many different authors. I kept the copyrights on each original file, but please message me if I missed someone.

You can help support these cores becoming a patron
https://www.patreon.com/vtrucco                           

Thanks to my patrons      
                          
Adam Stokes
Alencar Sucena            
Alexandre Guimaraes       
Alfredo Junior
Alfredo Tato   
André Conti                 
Andy McCall               
Andy Palmer               
Antonio Pereira  
Antonio Villena         
Alberto Maikuma  
Augusto Baffa
Ben Daluz
Bob Bazley          
Bruno Silva               
Carlos Krykhtine  
Cesar Cunha        
Chris Herbert   
Chris Millardi 
Chris Young 
Claudio Castro
D. ‘Xalior’ Rimron-Soutter    
Daniel Pellizzari  
Daniel Vitorino    
David Boocock             
David Powell              
David Saphier 
David Thurstan   
Dean Smith 
Dean Woodyatt         
Diogo Patrao              
Divino Leitao  
Duncan Corps           
Edson Kadoya    
Eirikur Sigbjornsson           
Emerson Cavallari  
Fabiano Barini
Fabio Cabral
Fernando Oliveira
Franco Bronda 
Gilberto Taborda Junior         
Henrique Olifiers         
Hugo Goncalves     
Human0Target    
James Shields
Jarvis Wahl Junior
Jon Arvid Borretzen    
Juliano Carlos de Oliveira
Luiz Tadeu Ramos   
Manoel Lemos              
Mario Azevedo             
Marcelo Eiras             
Marcelo Faria   
Marcio Lancellotti
Marek Szulen    
Mark Kirkby       
Mark Kohler (NML32)
Martin Boqvist 
Martin Dowie 
Marvin Malkowski Jr        
Matheus Santos      
Matthew Simon William Langtry      
Mauro Passarinho          
Mauricio Andrade          
Mike Cadwallader  
Mike Mee 
Miroslav Viavary
Mitja V. Iskric    
Neill Mitchell    
Paul Land    
Paulo Cacella             
Paulo Maluf Jr      
Paulo Vinicius Wolski Radtke              
Pedro Medeiros    
Peter Fitchett
Phil Harvey              
Phoebus Dokos             
Rafael Silva              
Renato Gomes    
Ricardo Michel
Richard Hallas      
Rob Morton    
Robert Berry  
Roberto Focosi
Roberto Lari      
Roberto Mota Besser      
Rodolfo Manoel
Rodrigo Bartole    
Rodrigo Fernandes
Rogerio Biondi        
Ronaldo Prado      
Sal Gunduz       
SeverinoCv Junior 
Shawn M
Simon Bowers
Simon N Goodwin
Stephen Cropp     
Steve Ingers
Tamas Vandorffy 
Tiago Bonamigo          
Tom Dalby                 
Tony Underwood            
Wanderley Ceschim 
Wayne Burton        
Will Stephenson        
Wilson Pilon              
Zoltan Boszormenyi        
                          
Thank you all!            