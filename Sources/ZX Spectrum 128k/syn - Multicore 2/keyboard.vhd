

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.keyscans.all;

entity keyboard is
port (
	CLK_I		: in std_logic;
	RESET_I		: in std_logic;
	
	ADDR_I		: in std_logic_vector(7 downto 0);
	KEYB_O		: out std_logic_vector(4 downto 0);
	KEYF_O		: out std_logic_vector(12 downto 1);
	KEYJOY_O	: out std_logic_vector(4 downto 0);
	KEYRESET_O	: out std_logic;
	
	PS2_CLK		: in std_logic;
	PS2_DATA		: in std_logic
	
	);
end keyboard;

architecture rtl of keyboard is

type key_matrix is array (11 downto 0) of std_logic_vector(4 downto 0);
signal keys		: key_matrix;
signal row0, row1, row2, row3, row4, row5, row6, row7 : std_logic_vector(4 downto 0);

signal keyb_data_s	: std_logic_vector(7 downto 0);
signal keyb_valid_s : std_logic := '0';
signal release  :   std_logic;
signal extended :   std_logic;
	
begin


 	ps2 : work.ps2_intf port map 
	(
		CLK			=> CLK_I,
		nRESET		=> not RESET_I,
	
		PS2_CLK		=> PS2_CLK,
		PS2_DATA		=> PS2_DATA,
	
		DATA			=> keyb_data_s,
		VALID			=> keyb_valid_s,
		ERROR			=> open
	);
	


	-- Output addressed row to ULA
	row0 <= keys(0) when ADDR_I(0) = '0' else (others => '1');
	row1 <= keys(1) when ADDR_I(1) = '0' else (others => '1');
	row2 <= keys(2) when ADDR_I(2) = '0' else (others => '1');
	row3 <= keys(3) when ADDR_I(3) = '0' else (others => '1');
	row4 <= keys(4) when ADDR_I(4) = '0' else (others => '1');
	row5 <= keys(5) when ADDR_I(5) = '0' else (others => '1');
	row6 <= keys(6) when ADDR_I(6) = '0' else (others => '1');
	row7 <= keys(7) when ADDR_I(7) = '0' else (others => '1');
	KEYB_O <= row0 and row1 and row2 and row3 and row4 and row5 and row6 and row7;

	KEYJOY_O 	<= keys(8);
	KEYRESET_O 	<= keys(11)(2);
	KEYF_O 		<= keys(11)(1) & keys(11)(0) & keys(10) & keys(9);
	
	process (RESET_I, CLK_I, keyb_data_s)
	begin
		if RESET_I = '1' then
			keys(0) <= (others => '1');
			keys(1) <= (others => '1');
			keys(2) <= (others => '1');
			keys(3) <= (others => '1');
			keys(4) <= (others => '1');
			keys(5) <= (others => '1');
			keys(6) <= (others => '1');
			keys(7) <= (others => '1');
			keys(8) <= (others => '0');
			keys(9) <= (others => '0');
			keys(10) <= (others => '0');
			keys(11) <= (others => '0');
			
		elsif CLK_I'event and CLK_I = '1' then
			if keyb_valid_s = '1' then
				if keyb_data_s = X"e0" then
					-- Extended key code follows
					extended <= '1';
				elsif keyb_data_s = X"f0" then
					-- Release code follows
					release <= '1';
				else
					-- Cancel extended/release flags for next time
					release <= '0';
					extended <= '0';

					if (extended = '0') then
					

						
						-- Normal scancodes
						case keyb_data_s is
						
							when KEY_LSHIFT		=> keys(0)(0) <= release; -- Left shift (CAPS SHIFT)
							when KEY_RSHIFT 		=> keys(0)(0) <= release; -- Right shift (CAPS SHIFT)
							when KEY_Z      		=> keys(0)(1) <= release; -- Z
							when KEY_X 				=> keys(0)(2) <= release; -- X
							when KEY_C 				=> keys(0)(3) <= release; -- C
							when KEY_V 				=> keys(0)(4) <= release; -- V

							when KEY_A 				=> keys(1)(0) <= release; -- A
							when KEY_S 				=> keys(1)(1) <= release; -- S
							when KEY_D 				=> keys(1)(2) <= release; -- D
							when KEY_F 				=> keys(1)(3) <= release; -- F
							when KEY_G 				=> keys(1)(4) <= release; -- G

							when KEY_Q 				=> keys(2)(0) <= release; -- Q
							when KEY_W 				=> keys(2)(1) <= release; -- W
							when KEY_E 				=> keys(2)(2) <= release; -- E
							when KEY_R 				=> keys(2)(3) <= release; -- R
							when KEY_T 				=> keys(2)(4) <= release; -- T

							when KEY_1 				=> keys(3)(0) <= release; -- 1
							when KEY_2 				=> keys(3)(1) <= release; -- 2
							when KEY_3 				=> keys(3)(2) <= release; -- 3
							when KEY_4 				=> keys(3)(3) <= release; -- 4
							when KEY_5 				=> keys(3)(4) <= release; -- 5

							when KEY_0 				=> keys(4)(0) <= release; -- 0
							when KEY_9 				=> keys(4)(1) <= release; -- 9
							when KEY_8 				=> keys(4)(2) <= release; -- 8
							when KEY_7 				=> keys(4)(3) <= release; -- 7
							when KEY_6 				=> keys(4)(4) <= release; -- 6

							when KEY_P 				=> keys(5)(0) <= release; -- P
							when KEY_O 				=> keys(5)(1) <= release; -- O
							when KEY_I 				=> keys(5)(2) <= release; -- I
							when KEY_U 				=> keys(5)(3) <= release; -- U
							when KEY_Y 				=> keys(5)(4) <= release; -- Y

							when KEY_ENTER 		=> keys(6)(0) <= release; -- ENTER
							when KEY_L 				=> keys(6)(1) <= release; -- L
							when KEY_K 				=> keys(6)(2) <= release; -- K
							when KEY_J 				=> keys(6)(3) <= release; -- J
							when KEY_H 				=> keys(6)(4) <= release; -- H

							when KEY_SPACE 		=> keys(7)(0) <= release; -- SPACE
							when KEY_LCTRL 		=> keys(7)(1) <= release; -- Left CTRL (Symbol Shift)
							when KEY_M 				=> keys(7)(2) <= release; -- M
							when KEY_N 				=> keys(7)(3) <= release; -- N
							when KEY_B 				=> keys(7)(4) <= release; -- B



						-- Other special keys sent to the ULA as key combinations
						when KEY_BACKSPACE =>
							keys(0)(0) <= release; -- Backspace (CAPS 0)
							keys(4)(0) <= release;
					--	when X"39" =>
					--		keys(0)(0) <= release; -- Caps lock (CAPS 2)
					--		keys(3)(1) <= release;
						when KEY_ESC =>
							keys(0)(0) <= release; -- Tab (CAPS SPACE)
							keys(7)(0) <= release;
				--		when X"37" =>
				--			keys(7)(2) <= release; -- .
				--			keys(7)(1) <= release;
				--		when X"2d" =>
				--			keys(6)(3) <= release; -- -
				--			keys(7)(1) <= release;
				--		when X"35" =>
				--			keys(3)(0) <= release; -- ` (EDIT)
				--			keys(0)(0) <= release;
				--		when X"36" =>
				--			keys(7)(3) <= release; -- ,
				--			keys(7)(1) <= release;
				--		when X"33" =>
				--			keys(5)(1) <= release; -- ;
				--			keys(7)(1) <= release;
				--		when X"34" =>
				--			keys(5)(0) <= release; -- "
				--			keys(7)(1) <= release;
				--		when X"31" =>
				--			keys(0)(1) <= release; -- :
				--			keys(7)(1) <= release;
				--		when X"2e" =>
				--			keys(6)(1) <= release; -- =
				--			keys(7)(1) <= release;
				--		when X"2f" =>
				--			keys(4)(2) <= release; -- (
				--			keys(7)(1) <= release;
				--		when X"30" =>
				--			keys(4)(1) <= release; -- )
				--			keys(7)(1) <= release;
				--		when X"38" =>
				--			keys(0)(3) <= release; -- ?
				--			keys(7)(1) <= release;
						--------------------------------------------
						-- Kempston keys
			--			when X"5e" => keys(8)(0) <= '1'; -- [6] (Right)
			--			when X"5c" => keys(8)(1) <= '1'; -- [4] (Left)
			--			when X"5a" => keys(8)(2) <= '1'; -- [2] (Down)
			--			when X"60" => keys(8)(3) <= '1'; -- [8] (Up)
			--			when X"62" => keys(8)(4) <= '1'; -- [0] (Fire)
				
						-- Soft keys
						when X"05" => keys(9)(0)  <= not release; -- F1
						when X"06" => keys(9)(1)  <= not release; -- F2
						when X"04" => keys(9)(2)  <= not release; -- F3
						when X"0c" => keys(9)(3)  <= not release; -- F4
						when X"03" => keys(9)(4)  <= not release; -- F5
						when X"0b" => keys(10)(0) <= not release; -- F6
						when X"83" => keys(10)(1) <= not release; -- F7
						when X"0a" => keys(10)(2) <= not release; -- F8
						when X"01" => keys(10)(3) <= not release; -- F9
						when X"09" => keys(10)(4) <= not release; -- F10
						when X"78" => keys(11)(0) <= not release; -- F11
						when X"07" => keys(11)(1) <= not release; -- F12
						 
						 

			
						-- Hardware keys
		--				when X"47" => keys(11)(2) <= '1'; -- Scroll Lock (RESET_I)
		--				when X"48" => keys(11)(3) <= '1'; -- Pause
		--				when X"65" => keys(11)(4) <= '1'; -- WinMenu
										
						when others => null;
					end case;
				else
						-- Extended scancodes
						case keyb_data_s is

							-- Cursor keys
							
							when KEY_KPENTER 		=> -- ENTER
								keys(6)(0) <= release; 
								
							when KEY_LEFT			=>	 -- Left (CAPS 5)
								keys(0)(0) <= release; 
								keys(3)(4) <= release;
							
							when KEY_DOWN			=>	-- Down (CAPS 6)
								keys(0)(0) <= release; 
								keys(4)(4) <= release;
							
							when KEY_UP				=>	-- Up (CAPS 7)
								keys(0)(0) <= release; -- Up (CAPS 7)
								keys(4)(3) <= release;
								
							when KEY_RIGHT			=>	 -- Right (CAPS 8)
								keys(0)(0) <= release; -- Right (CAPS 8)
								keys(4)(2) <= release;								

							when others =>
								null;
						end case;
					end if;
				end if;
			end if;
		end if;
	end process;

end architecture;
