-----------------------------------------------------------------------------
--
--  ZX Spectrum Next top by Victor Trucco 2020
--
-----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;

library UNISIM;  
use UNISIM.Vcomponents.all;

entity top is
	port (
		-- Clocks
		clock_50_i			: in    std_logic;

		-- SRAM (AS7C34096)
		ram_addr_o			: out   std_logic_vector(18 downto 0)	:= (others => '0');
		ram_data_io			: inout std_logic_vector(15 downto 0)	:= (others => 'Z');
		ram_oe_n_o			: out   std_logic								:= '1';
		ram_we_n_o			: out   std_logic								:= '1';
		ram_ce_n_o			: out   std_logic_vector( 3 downto 0)	:= (others => '1');

		-- PS2
		ps2_clk_io			: inout std_logic								:= 'Z';
		ps2_data_io			: inout std_logic								:= 'Z';
		ps2_pin6_io			: inout std_logic								:= 'Z';	-- Mouse clock
		ps2_pin2_io 		: inout std_logic								:= 'Z';	-- Mouse data

		-- SD Card
		sd_cs0_n_o			: out   std_logic								:= '1';
		sd_cs1_n_o			: out   std_logic								:= '1';
		sd_sclk_o			: out   std_logic								:= '0';
		sd_mosi_o			: out   std_logic								:= '0';
		sd_miso_i			: in    std_logic;

		-- Flash
		flash_cs_n_o		: out   std_logic								:= '1';
		flash_sclk_o		: out   std_logic								:= '0';
		flash_mosi_o		: out   std_logic								:= '0';
		flash_miso_i		: in    std_logic;
		flash_wp_o			: out   std_logic								:= '0';
		flash_hold_o		: out   std_logic								:= '1';

		-- Joystick
		joyp1_i				: in    std_logic;
		joyp2_i				: in    std_logic;
		joyp3_i				: in    std_logic;
		joyp4_i				: in    std_logic;
		joyp6_i				: in    std_logic;
		joyp7_o				: out   std_logic								:= '1';
		joyp9_i				: in    std_logic;
		joysel_o				: out   std_logic								:= '0';

		-- Audio
		audioext_l_o		: out   std_logic								:= '0';
		audioext_r_o		: out   std_logic								:= '0';
		audioint_o			: out   std_logic								:= '0';

		-- K7
		ear_port_i			: in    std_logic;
		mic_port_o			: out   std_logic								:= '0';

		-- Buttons
		btn_divmmc_n_i		: in    std_logic;
		btn_multiface_n_i	: in    std_logic;
		btn_reset_n_i		: in    std_logic;

		-- Matrix keyboard
		keyb_row_o			: out   std_logic_vector( 7 downto 0)	:= (others => '1');
		keyb_col_i			: in    std_logic_vector( 6 downto 0);

		-- Bus
		bus_rst_n_io		: inout std_logic								:= 'Z';
		bus_clk35_o			: out   std_logic								:= '0';
		bus_addr_o			: out   std_logic_vector(15 downto 0)	:= (others => '0');
		bus_data_io			: inout std_logic_vector( 7 downto 0)	:= (others => 'Z');
		bus_int_n_i			: in    std_logic;
		bus_nmi_n_i			: in    std_logic;
		bus_ramcs_i			: in    std_logic;
		bus_romcs_i			: in    std_logic;
		bus_wait_n_i		: in    std_logic;
		bus_halt_n_o		: out   std_logic								:= '1';
		bus_iorq_n_o		: out   std_logic								:= '1';
		bus_m1_n_o			: out   std_logic								:= '1';
		bus_mreq_n_o		: out   std_logic								:= '1';
		bus_rd_n_o			: out   std_logic								:= '1';
		bus_wr_n_o			: out   std_logic								:= '1';
		bus_rfsh_n_o		: out   std_logic								:= '1';
		bus_busreq_n_i		: in    std_logic;
		bus_busack_n_o		: out   std_logic								:= '1';
		bus_iorqula_n_i	: in    std_logic;

		-- VGA
		rgb_r_o				: out   std_logic_vector( 2 downto 0)	:= (others => '0');
		rgb_g_o				: out   std_logic_vector( 2 downto 0)	:= (others => '0');
		rgb_b_o				: out   std_logic_vector( 2 downto 0)	:= (others => '0');
		hsync_o				: out   std_logic								:= '1';
		vsync_o				: out   std_logic								:= '1';
		csync_o				: out   std_logic								:= '1';

		-- HDMI
		hdmi_p_o				: out   std_logic_vector(3 downto 0);
		hdmi_n_o				: out   std_logic_vector(3 downto 0);
--		hdmi_cec_i			: in    std_logic;

		-- I2C (RTC and HDMI)
		i2c_scl_io			: inout std_logic								:= 'Z';
		i2c_sda_io			: inout std_logic								:= 'Z';

		-- ESP
		esp_gpio0_io		: inout std_logic								:= 'Z';
		esp_gpio2_io		: inout std_logic								:= 'Z';
		esp_rx_i				: in    std_logic;
		esp_tx_o				: out   std_logic								:= '0';

		-- ACCELERATOR BOARD
		accel_io_27		: out std_logic;
		accel_io_26		: out std_logic;
		accel_io_25		: out std_logic;
		accel_io_24		: out std_logic;
		accel_io_23		: out std_logic;
		accel_io_22		: out std_logic;
		accel_io_21		: out std_logic;
		accel_io_20		: out  std_logic;
		accel_io_19		: out  std_logic;
		accel_io_18		: out  std_logic;
		accel_io_17		: out  std_logic;
		accel_io_16		: out  std_logic;
		accel_io_15		: out  std_logic;
		accel_io_14		: out  std_logic;
		accel_io_13		: out  std_logic;
		accel_io_12		: out  std_logic;
		accel_io_11		: out  std_logic;
		accel_io_10		: out  std_logic;
		accel_io_9		: out  std_logic;
		accel_io_8		: out std_logic;
		accel_io_7		: out std_logic;
		accel_io_6		: out std_logic;
		accel_io_5		: out std_logic;
		accel_io_4		: out std_logic;
		accel_io_3		: out std_logic;
		accel_io_2		: out std_logic;
		accel_io_1		: out std_logic;
		accel_io_0		: out std_logic;
		
		-- Vacant pins
		extra_io			: out std_logic
		
	);
end entity;

architecture Behavior of top is

	-- Reset signal
	signal reset_n				: std_logic;		-- Reset geral
	
	-- Master clock
	signal pll_reset			: std_logic;		-- Reset do PLL
	signal pll_locked			: std_logic;		-- PLL travado quando 1
	
	-- Master clock
	signal clk_28		: std_logic;
	--signal odyssey_clk		: std_logic;
	signal vid_clk: std_logic := '0';
	signal sysclk : std_logic := '0';
	
	
	signal port_243b : std_logic_vector(7 downto 0);
	
	signal ram_a				: std_logic_vector(17 downto 0);		-- 512K
	signal ram_din				: std_logic_vector(15 downto 0);
	signal ram_dout			: std_logic_vector(15 downto 0);
	signal ram_cs				: std_logic;
	signal ram_oe				: std_logic;
	signal ram_we				: std_logic;
	signal rom_a				: std_logic_vector(13 downto 0);		-- 16K
	signal rom_dout			: std_logic_vector(7 downto 0);
	
	signal from_sram			: std_logic_vector(15 downto 0);
	signal to_sram				: std_logic_vector(15 downto 0);
	
		-- ram
	signal loader_ram_a		: std_logic_vector(17 downto 0);		-- 512K
	signal loader_to_sram	: std_logic_vector(15 downto 0);
	signal loader_from_sram	: std_logic_vector(15 downto 0);
	signal loader_ram_data	: std_logic_vector(15 downto 0);
	signal loader_ram_cs		: std_logic;
	signal loader_ram_oe		: std_logic;
	signal loader_ram_we		: std_logic;

	signal a2601_ram_a		: std_logic_vector(11 downto 0);	
	signal a2601_ram_dout	: std_logic_vector(7 downto 0);
	

	-- scanlines
	signal scanlines_en_s		: std_logic := '0';
	signal btn_scan_s				: std_logic;
	signal odd_line_s				: std_logic := '0';
	
	
	--rgb
	signal rgb_loader_out		: std_logic_vector(7 downto 0);
	signal rgb_odyssey_out		: std_logic_vector(7 downto 0);
	
	signal hsync_loader_out		: std_logic;
	signal vsync_loader_out		: std_logic;
	
	signal hsync_odyssey_out	: std_logic;
	signal vsync_odyssey_out	: std_logic;
	
	signal cart_a        		: std_logic_vector(12 downto 0);
	signal cart_d 					: std_logic_vector( 7 downto 0);
  
	signal odyssey_reset_n_s 	: std_logic;
          
	-- PS/2
	signal keyb_data_s 			: std_logic_vector(7 downto 0);
	signal keyb_valid_s 			: std_logic;
	
	-- HDMI
	signal tdms_r_s				: std_logic_vector( 9 downto 0);
	signal tdms_g_s				: std_logic_vector( 9 downto 0);
	signal tdms_b_s				: std_logic_vector( 9 downto 0);
	signal hdmi_p_s				: std_logic_vector( 3 downto 0);
	signal hdmi_n_s				: std_logic_vector( 3 downto 0);
	
	signal clk_active				: std_logic;
	signal clk_vga					: std_logic;
	signal clk_dvi					: std_logic;
	signal clk_dvi_180			: std_logic;
	
	signal color_s 				: std_logic_vector(3 downto 0);
	signal vga_color_s 			: std_logic_vector(3 downto 0);
	signal vga_hsync_n_s 		: std_logic;
	signal vga_vsync_n_s 		: std_logic;
	signal vga_blank_s 			: std_logic;
	
	signal cnt_hor_s 				: std_logic_vector(8 downto 0);
	signal cnt_ver_s 				: std_logic_vector(8 downto 0);
	signal sound_hdmi_s			: std_logic_vector(15 downto 0);
	signal tdms_s					: std_logic_vector( 7 downto 0);
	
	signal loader_hor_s 			: std_logic_vector(8 downto 0);
	signal loader_ver_s 			: std_logic_vector(8 downto 0);
	signal vp_hor_s 				: std_logic_vector(8 downto 0);
	signal vp_ver_s 				: std_logic_vector(8 downto 0);
	signal vp_color_index_s 	: std_logic_vector(3 downto 0);
	signal vga_rgb_s 				: std_logic_vector(7 downto 0);

	signal vga_rgb_out_s 		: std_logic_vector(7 downto 0);
	
	signal snd_vec_s				: std_logic_vector(3 downto 0);
	signal audio_s 				: std_logic;
	
	-- Joystick
	signal joy1_s				: std_logic_vector( 5 downto 0);
	signal joy2_s				: std_logic_vector( 5 downto 0);
	signal joysel_s			: std_logic;
	signal btn_scandb_s		: std_logic;

	signal power_on_reset	: std_logic := '0';
	signal power_on_s			: std_logic_vector(24 downto 0) := (others=>'1');

	signal osd_s				: std_logic_vector(4 downto 0) := (others=>'0');	
	
	signal scandblctrl 		: std_logic := '1';
begin

	-------- clocks
	pll: entity work.pll_odyssey 
	port map (
		CLK_IN1		=> clock_50_i,				-- Clock 50 MHz externo
		CLK_OUT1		=> sysclk,					
		CLK_OUT2		=> vid_clk,					
		CLK_OUT3		=> clk_28,		
		CLK_OUT4		=> clk_vga,			
		CLK_OUT5		=> clk_dvi,	
		CLK_OUT6		=> clk_dvi_180,			
		LOCKED		=> pll_locked				-- Sinal de travamento (1 = PLL travado e pronto)
	);
	
	pll_reset	<= '1' when btn_divmmc_n_i = '0' else '0';
	reset_n		<= not (pll_reset or not pll_locked);	-- System is reset by external reset switch or PLL being out of lock
	-----------------------------
	
	-------- joystick multiplex
	process (clk_active)
	begin
		if rising_edge(clk_active) then
			joysel_s <= not joysel_s;
			
			if joysel_s = '0' then
				joy1_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			else
			   joy2_s <= joyp9_i & joyp6_i & joyp4_i & joyp3_i & joyp2_i & joyp1_i;
			end if;
			
			
		end if;
	end process;
	
	joysel_o <= joysel_s;
	-----------------------------

	-------- keyboard
 	ps2 : entity work.ps2_intf port map 
	(
		clk_active,
		reset_n,
		ps2_clk_io,
		ps2_data_io,
		keyb_data_s,
		keyb_valid_s,
		open
	);
	
	
	cpuclk_selector : BUFGMUX  
	port map
	(
		S	=> odyssey_reset_n_s,
		I0	=> clk_28,
		I1	=> sysclk,
		O	=> clk_active
    );

	--clk_keyb <= clk_28 when odyssey_reset_n_s = '0' else sysclk;
	

	-----------------------------


	-------- cartridge loader
	
	loader : entity work.speccy48_top 
	port map 
	(
		
		clk_28    => clk_28,
		reset_n_i => reset_n and (not power_on_reset),
	
		-- VGA
		VGA_R(3 downto 1) => rgb_loader_out (7 downto 5),    
		VGA_R(0) 			=> open,    
		VGA_G(3 downto 1) => rgb_loader_out (4 downto 2),    
		VGA_G(0) 			=> open,   
		VGA_B(3 downto 2) => rgb_loader_out (1 downto 0),   
		VGA_B(1 downto 0)	=> open,   
		VGA_HS         	=> hsync_loader_out,  
		VGA_VS         	=> vsync_loader_out,   

		
		-- PS/2 Keyboar   -- PS/2 Ke
		keyb_data      => keyb_data_s, 
		keyb_valid     => keyb_valid_s,  
   
   
		SRAM_ADDR      => loader_ram_a,
		FROM_SRAM      => loader_from_sram,
		TO_SRAM       	=> loader_to_sram,
		SRAM_CE_N      => loader_ram_cs,
		SRAM_OE_N      => loader_ram_oe,
		SRAM_WE_N      => loader_ram_we,
                  
				  
		SD_nCS         => sd_cs0_n_o,
		SD_MOSI        => sd_mosi_o, 
		SD_SCLK        => sd_sclk_o, 
		SD_MISO        => sd_miso_i,
	
		PORT_243B      => port_243b,
		
		JOYSTICK			=> "000" & ((not (joy1_s(4) and joy2_s(4) and joy1_s(5) and joy2_s(5)) &  not (joy1_s(0) and joy2_s(0)) &  not (joy1_s(1) and joy2_s(1)) & not (joy1_s(2) and joy2_s(2)) & not (joy1_s(3) and joy2_s(3))) or osd_s),
		
		cnt_h_o      	=> loader_hor_s,
		cnt_v_o      	=> loader_ver_s
		
	);
	
--	ram_ce_n_o   <= "111" & ram_cs;
--	ram_oe_n_o   <= ram_oe;
	ram_ce_n_o   <= "1110" ;
	ram_oe_n_o   <= '0';
	
	
	ram_addr_o   <= "0" & ram_a;
	ram_we_n_o   <= ram_we;

	ram_data_io(15 downto 8) <= (others=>'Z');
	ram_data_io(7 downto 0)  <= to_sram(7 downto 0) when ram_we = '0' else (others=>'Z');
	from_sram(7 downto 0) 	 <= ram_data_io(7 downto 0);
	-----------------------------
	
	

	
	odyssey2: entity work.top_odyssey2 port map (
  
	-- Clocks
		clk_s					=> sysclk, --odyssey_clk,

		reset_n   			=> odyssey_reset_n_s,
		
		btn_reset_n_i 		=> btn_reset_n_i,

		-- PS2
		keyb_data     		=> keyb_data_s, 
		keyb_valid     	=> keyb_valid_s,  

		-- Joystick
		joy1_up_i			=> joy1_s(0),
		joy1_down_i			=> joy1_s(1),
		joy1_left_i			=> joy1_s(2),
		joy1_right_i		=> joy1_s(3),
		joy1_p6_i			=> joy1_s(4),
		joy1_p7_o			=> joyp7_o,
		joy1_p9_i			=> joy1_s(5),
		joy2_up_i			=> joy2_s(0),
		joy2_down_i			=> joy2_s(1),
		joy2_left_i			=> joy2_s(2),
		joy2_right_i		=> joy2_s(3),
		joy2_p6_i			=> joy2_s(4),
		joy2_p7_o			=> open,
		joy2_p9_i			=> joy2_s(5),

		-- Audio
		snd_vec_o			=> snd_vec_s,


		-- VGA
		vga_r_o				=> rgb_odyssey_out(7 downto 5),
		vga_g_o				=> rgb_odyssey_out(4 downto 2),
		vga_b_o(2 downto 1)=> rgb_odyssey_out(1 downto 0),
		vga_b_o(0)			=> open,
		vga_hsync_n_o		=> hsync_odyssey_out,
		vga_vsync_n_o		=> vsync_odyssey_out,
		
		color_index_o => vp_color_index_s,


		-- Debug
		leds_n_o				=> open,
		
		-- cartridge
		rom_a_s => cart_a,
		rom_d_s => cart_d,
		
		hpos_o     => vp_hor_s,
		vpos_o  	  => vp_ver_s,
		
		-- membrane keyboard
		keyb_row_o		=> keyb_row_o,
		keyb_col_i		=> keyb_col_i,
		osd_o				=> osd_s
    
  );

	  

	
--	cartridge : entity work.cart
--	port map
--	(
--		CLKA	=> sysclk,
--		ADDRA => cart_a,
--		DOUTA => cart_d
--	);

	---------------------------------------------------
	

	process (port_243b)
	begin
		if port_243b(7) = '1' then --magic bit to start 
				
				--ODYSSEY 2
				
				--odyssey_clk 	<= sysclk;
				ram_a				<= "00100" & cart_a;
				to_sram			<= (others=>'Z');
				cart_d			<= from_sram(7 downto 0);
				ram_cs			<= '0';
				ram_oe			<= '0';
				ram_we			<= '1';
				
				color_s <=  vp_color_index_s; --"0" & rgb_odyssey_out(7)& rgb_odyssey_out(4)& rgb_odyssey_out(1);
				
				odyssey_reset_n_s <= '1';
				
--				clk_active <= sysclk;
				
				cnt_hor_s <= vp_hor_s - 15; -- - 20;-- -25; 
				cnt_ver_s <= vp_ver_s - 8; -- -15 is too much
				
				sound_hdmi_s <= "0000" & snd_vec_s & "00000000";
			
		else
			
				--LOADER

				ram_a				<= loader_ram_a;
				to_sram			<= loader_to_sram;
				loader_from_sram <= from_sram;
				ram_cs			<= loader_ram_cs;
				ram_oe			<= loader_ram_oe;
				ram_we			<= loader_ram_we;
				
				color_s  <= "0" & rgb_loader_out(7)& rgb_loader_out(4)& rgb_loader_out(1);
				
				odyssey_reset_n_s <= '0';
				
--				clk_active <= clk_28;
				cnt_hor_s <= loader_hor_s;
				cnt_ver_s <= loader_ver_s;
				
				sound_hdmi_s <=(others=>'0');
			
		end if;
	end process;
	
	

	
	-------- Sound
	  dac_b : entity work.dac
    generic map (
      msbi_g => 7
    )
    port map (
      clk_i   => clk_active,
      res_n_i => reset_n,
      dac_i   => "0" & snd_vec_s & "000",
      dac_o   => audio_s
    );
  --
  audioext_l_o <= audio_s;
  audioext_r_o <= audio_s;
	-----------------------------	
	
	-------- Video 
	vga: entity work.vga
	port map (
		I_CLK			=> clk_active,
		I_CLK_VGA	=> clk_vga,
		I_COLOR		=> color_s,
		I_HCNT		=> cnt_hor_s,
		I_VCNT		=> cnt_ver_s,
		O_HSYNC		=> vga_hsync_n_s,
		O_VSYNC		=> vga_vsync_n_s,
		O_COLOR		=> vga_color_s,
		O_HCNT		=> open,
		O_VCNT		=> open,
		O_H			=> open,
		O_BLANK		=> vga_blank_s
	);
	
	
	process (vga_color_s)
	begin
		case vga_color_s is
			when "0000" => vga_rgb_s <= "00000000"; --X"000000"; -- 000 000 000
			when "0001" => vga_rgb_s <= "00000111"; --X"0e3dd4"; -- 000 001 110
			when "0010" => vga_rgb_s <= "00010000"; --X"00981b"; -- 000 100 000
			when "0011" => vga_rgb_s <= "00010111"; --X"00bbd9"; -- 000 101 110
			when "0100" => vga_rgb_s <= "11000000"; --X"c70008"; -- 110 000 000
			when "0101" => vga_rgb_s <= "11000010"; --X"cc16b3"; -- 110 000 101
			when "0110" => vga_rgb_s <= "10010000"; --X"9d8710"; -- 100 100 000
			when "0111" => vga_rgb_s <= "11111011"; --X"e1dee1"; -- 111 110 111
			when "1000" => vga_rgb_s <= "01001101"; --X"5f6e6b"; -- 010 011 011
			when "1001" => vga_rgb_s <= "01110111"; --X"6aa1ff"; -- 011 101 111
			when "1010" => vga_rgb_s <= "00111101"; --X"3df07a"; -- 001 111 011
			when "1011" => vga_rgb_s <= "00111111"; --X"31ffff"; -- 001 111 111
			when "1100" => vga_rgb_s <= "11101001"; --X"ff4255"; -- 111 010 010
			when "1101" => vga_rgb_s <= "11110011"; --X"ff98ff"; -- 111 100 111
			when "1110" => vga_rgb_s <= "11010101"; --X"d9ad5d"; -- 110 101 010
			when "1111" => vga_rgb_s <= "11111111"; --X"ffffff"; -- 111 111 111
			when others => vga_rgb_s <= "00000001"; --X"000000"; -- 000 000 000
		end case;
	end process;
	
			-- HDMI
		inst_dvid: entity work.hdmi
		generic map (
			FREQ	=> 25000000,	-- pixel clock frequency 
			FS		=> 48000,		-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
			CTS	=> 25000,		-- CTS = Freq(pixclk) * N / (128 * Fs)
			N		=> 6144			-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
		) 
		port map (
 			I_CLK_PIXEL		=> clk_vga,
			
			I_R				=> vga_rgb_out_s(7 downto 5) & vga_rgb_out_s(7 downto 5) & vga_rgb_out_s(7 downto 6),
			I_G				=> vga_rgb_out_s(4 downto 2) & vga_rgb_out_s(4 downto 2) & vga_rgb_out_s(4 downto 3),
			I_B				=> vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(1 downto 0),
			
			I_BLANK			=> vga_blank_s,
			I_HSYNC			=> vga_hsync_n_s,
			I_VSYNC			=> vga_vsync_n_s,
			
			-- PCM audio
			I_AUDIO_ENABLE	=> '1',
 			I_AUDIO_PCM_L 	=> sound_hdmi_s,
 			I_AUDIO_PCM_R	=> sound_hdmi_s,
			
			-- TMDS parallel pixel synchronous outputs (serialize LSB first)
 			O_RED				=> tdms_r_s,
			O_GREEN			=> tdms_g_s,
			O_BLUE			=> tdms_b_s
		);
		

		hdmio: entity work.hdmi_out_xilinx
		port map (
			clock_pixel_i		=> clk_vga,
			clock_tdms_i		=> clk_dvi,
			clock_tdms_n_i		=> clk_dvi_180,
			red_i					=> tdms_r_s,
			green_i				=> tdms_g_s,
			blue_i				=> tdms_b_s,
			tmds_out_p			=> hdmi_p_o,
			tmds_out_n			=> hdmi_n_o
		);

		-- VGA
		rgb_r_o			<= vga_rgb_out_s(7 downto 5);
		rgb_g_o			<= vga_rgb_out_s(4 downto 2);
		rgb_b_o			<= vga_rgb_out_s(1 downto 0) & vga_rgb_out_s(0);
		hsync_o			<= vga_hsync_n_s;
		vsync_o			<= vga_vsync_n_s;
	
	---------------------------------
	-- scanlines
	btnscl: entity work.debounce
	generic map (
		counter_size_g	=> 16
	)
	port map (
		clk_i				=> clk_28,
		button_i			=> btn_multiface_n_i,
		result_o			=> btn_scan_s
	);
	
	process (btn_scan_s)
	begin
		if falling_edge(btn_scan_s) then
			scanlines_en_s <= not scanlines_en_s;
		end if;
	end process;
	
	
	vga_rgb_out_s <= '0' & vga_rgb_s(6 downto 5) & '0' & vga_rgb_s(3 downto 2)& '0' & vga_rgb_s(0) when scanlines_en_s = '1' and odd_line_s = '1' else vga_rgb_s;
	
	
	
	process(vga_hsync_n_s,vga_vsync_n_s)
	begin
		if vga_vsync_n_s = '0' then
			odd_line_s <= '0';
		elsif rising_edge(vga_hsync_n_s) then
			odd_line_s <= not odd_line_s;
		end if;
	end process;
	
	--reset power on
		process (sysclk)
		begin
			if rising_edge(sysclk) then
				if pll_locked = '0' then
					power_on_s <= (others=>'1');
				elsif power_on_s /= '0' & x"000000" then
					power_on_s <= power_on_s - 1;
				--	power_on_reset <= '1';
			--	else
			--		power_on_reset <= '0';
					
				end if;
				
				power_on_reset <= power_on_s(23);
			end if;
		end process;
		
--------------------------------------------------------
-- Unused outputs
--------------------------------------------------------
 
	-- Rpi port
	accel_io_0     <= 'Z';
	accel_io_1     <= 'Z';
	accel_io_2     <= 'Z';
	accel_io_3     <= 'Z';
	accel_io_4     <= 'Z';
	accel_io_5     <= 'Z';
	accel_io_6     <= 'Z';
	accel_io_7     <= 'Z';
	accel_io_8     <= 'Z';
	accel_io_9     <= 'Z';
	accel_io_10    <= 'Z';
	accel_io_11    <= 'Z';
	accel_io_12    <= 'Z';
	accel_io_13    <= 'Z';
	accel_io_14    <= 'Z';
	accel_io_15    <= 'Z';
	accel_io_16    <= 'Z';
	accel_io_17    <= 'Z';
	accel_io_18    <= 'Z';
	accel_io_19    <= 'Z';
	accel_io_20    <= 'Z';
	accel_io_21    <= 'Z';
	accel_io_22    <= 'Z';
	accel_io_23    <= 'Z';
	accel_io_24    <= 'Z';
	accel_io_25    <= 'Z';
	accel_io_26    <= 'Z';
	accel_io_27    <= 'Z';

	extra_io    <= 'Z';
    -- Interal audio (speaker, not fitted)
    audioint_o     <= '0';

    -- Spectrum Next Bus
    bus_addr_o     <= x"0000";
    bus_busack_n_o <= '1';
    bus_clk35_o    <= '1';
    bus_data_io    <= "ZZZZZZZZ";
    bus_halt_n_o   <= '1';
    bus_iorq_n_o   <= '1';
    bus_m1_n_o     <= '1';
    bus_mreq_n_o   <= '1';
    bus_rd_n_o     <= '1';
    bus_rfsh_n_o   <= '1';
    bus_rst_n_io   <= 'Z';
    bus_wr_n_o     <= '1';

    -- TODO: add support for sRGB output
    csync_o        <= '1';

    -- ESP 8266 module
    esp_gpio0_io   <= 'Z';
    esp_gpio2_io   <= 'Z';

    -- Addtional flash pins; used at IO2 and IO3 in Quad SPI Mode
    flash_hold_o   <= 'Z';
    flash_wp_o     <= 'Z';

    i2c_scl_io <= 'Z';
    i2c_sda_io <= 'Z';

    -- Mic Port (output, as it connects to the mic input on cassette deck)
    mic_port_o <= '0';

    -- CS1 is for internal SD socket
    sd_cs1_n_o <= '1';

end architecture;
