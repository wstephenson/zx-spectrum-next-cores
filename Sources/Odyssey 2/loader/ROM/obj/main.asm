;--------------------------------------------------------
; File Created by SDCC : free open source ANSI-C Compiler
; Version 3.6.0 #9615 (MINGW32)
;--------------------------------------------------------
	.module main
	.optsdcc -mz80
	
;--------------------------------------------------------
; Public variables in this module
;--------------------------------------------------------
	.globl _main
	.globl _draw_menu
	.globl _NextDirEntry
	.globl _ChangeDirectory
	.globl _FileRead
	.globl _FileOpen
	.globl _FindDrive
	.globl _MMC_Init
	.globl _vdp_prints
	.globl _vdp_gotoxy
	.globl _vdp_cls
	.globl _vdp_setbg
	.globl _vdp_setcolor
	.globl _vdp_init
;--------------------------------------------------------
; special function registers
;--------------------------------------------------------
_SD_CONTROL	=	0x00e7
_SD_STATUS	=	0x00e7
_SD_DATA	=	0x00eb
_ULAPORT	=	0x00fe
_REG_NUM	=	0x243b
_REG_VAL	=	0x253b
_JOYPORT	=	0x263b
_HROW0	=	0xfefe
_HROW1	=	0xfdfe
_HROW2	=	0xfbfe
_HROW3	=	0xf7fe
_HROW4	=	0xeffe
_HROW5	=	0xdffe
_HROW6	=	0xbffe
_HROW7	=	0x7ffe
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _DATA
_cursor:
	.ds 1
_keys:
	.ds 1
_romindex:
	.ds 2
_romfilenames:
	.ds 540
_extension:
	.ds 10
;--------------------------------------------------------
; ram data
;--------------------------------------------------------
	.area _INITIALIZED
;--------------------------------------------------------
; absolute external ram data
;--------------------------------------------------------
	.area _DABS (ABS)
;--------------------------------------------------------
; global & static initialisations
;--------------------------------------------------------
	.area _HOME
	.area _GSINIT
	.area _GSFINAL
	.area _GSINIT
;--------------------------------------------------------
; Home
;--------------------------------------------------------
	.area _HOME
	.area _HOME
;--------------------------------------------------------
; code
;--------------------------------------------------------
	.area _CODE
;src/main.c:92: static void display_error(unsigned char *message)
;	---------------------------------
; Function display_error
; ---------------------------------
_display_error:
;src/main.c:96: vdp_cls();
	call	_vdp_cls
;src/main.c:97: ULAPORT = COLOR_RED;
	ld	a,#0x02
	out	(_ULAPORT),a
;src/main.c:98: vdp_gotoxy( 0, 12 );
	ld	hl,#0x0c00
	push	hl
	call	_vdp_gotoxy
	pop	af
;src/main.c:99: vdp_prints(message);
	pop	bc
	pop	hl
	push	hl
	push	bc
	push	hl
	call	_vdp_prints
	pop	af
;src/main.c:102: for(i=0; i<65000; i++);
	ld	bc,#0xfde8
00105$:
	ld	e,c
	ld	d,b
	dec	de
	ld	c, e
	ld	a,d
	ld	b,a
	or	a,e
	jr	NZ,00105$
;src/main.c:104: for(k=0; k<65000; k++);
	ld	bc,#0xfde8
00108$:
	ld	e,c
	ld	d,b
	dec	de
	ld	c, e
	ld	a,d
	ld	b,a
	or	a,e
	jr	NZ,00108$
;src/main.c:106: j=i;
	ret
;src/main.c:111: static void draw_cursor()
;	---------------------------------
; Function draw_cursor
; ---------------------------------
_draw_cursor:
;src/main.c:113: vdp_gotoxy( 0, cursor + 4 );
	ld	a,(#_cursor + 0)
	add	a, #0x04
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_vdp_gotoxy
;src/main.c:114: vdp_prints( "-->" );
	ld	hl, #___str_0
	ex	(sp),hl
	call	_vdp_prints
	pop	af
	ret
___str_0:
	.ascii "-->"
	.db 0x00
;src/main.c:117: static void hide_cursor()
;	---------------------------------
; Function hide_cursor
; ---------------------------------
_hide_cursor:
;src/main.c:119: vdp_gotoxy( 0, cursor + 4 );
	ld	a,(#_cursor + 0)
	add	a, #0x04
	push	af
	inc	sp
	xor	a, a
	push	af
	inc	sp
	call	_vdp_gotoxy
;src/main.c:120: vdp_prints( "   " );
	ld	hl, #___str_1
	ex	(sp),hl
	call	_vdp_prints
	pop	af
	ret
___str_1:
	.ascii "   "
	.db 0x00
;src/main.c:123: static void readkeyb()
;	---------------------------------
; Function readkeyb
; ---------------------------------
_readkeyb:
;src/main.c:128: keys = 0;
	ld	hl,#_keys + 0
	ld	(hl), #0x00
;src/main.c:130: while(1) {
00164$:
;src/main.c:137: if ((HROW3 & 0x10) == 0) //left
	ld	a,#>(_HROW3)
	in	a,(#<(_HROW3))
	and	a, #0x10
	jr	NZ,00105$
;src/main.c:139: keys = 0x02;
	ld	hl,#_keys + 0
	ld	(hl), #0x02
;src/main.c:140: while(!(HROW3 & 0x10));
00101$:
	ld	a,#>(_HROW3)
	in	a,(#<(_HROW3))
	and	a, #0x10
	jr	Z,00101$
;src/main.c:141: return;
	ret
00105$:
;src/main.c:143: t = HROW4;
	ld	a,#>(_HROW4)
	in	a,(#<(_HROW4))
;src/main.c:144: if ((t & 0x10) == 0) //down
	bit	4, a
	jr	NZ,00110$
;src/main.c:146: keys = 0x04;
	ld	hl,#_keys + 0
	ld	(hl), #0x04
;src/main.c:147: while(!(HROW4 & 0x10));
00106$:
	ld	a,#>(_HROW4)
	in	a,(#<(_HROW4))
	and	a, #0x10
	jr	Z,00106$
;src/main.c:148: return;
	ret
00110$:
;src/main.c:150: if ((t & 0x08) == 0) //up
	bit	3, a
	jr	NZ,00115$
;src/main.c:152: keys = 0x08;
	ld	hl,#_keys + 0
	ld	(hl), #0x08
;src/main.c:153: while(!(HROW4 & 0x08));
00111$:
	ld	a,#>(_HROW4)
	in	a,(#<(_HROW4))
	and	a, #0x08
	jr	Z,00111$
;src/main.c:154: return;
	ret
00115$:
;src/main.c:156: if ((t & 0x04) == 0) //right
	bit	2, a
	jr	NZ,00120$
;src/main.c:158: keys = 0x01;
	ld	hl,#_keys + 0
	ld	(hl), #0x01
;src/main.c:159: while(!(HROW4 & 0x04));
00116$:
	ld	a,#>(_HROW4)
	in	a,(#<(_HROW4))
	and	a, #0x04
	jr	Z,00116$
;src/main.c:160: return;
	ret
00120$:
;src/main.c:162: if ((HROW7 & 0x01) == 0) //space
	ld	a,#>(_HROW7)
	in	a,(#<(_HROW7))
	rrca
	jr	C,00125$
;src/main.c:164: keys = 0x10;
	ld	hl,#_keys + 0
	ld	(hl), #0x10
;src/main.c:165: while(!(HROW7 & 0x01));
00121$:
	ld	a,#>(_HROW7)
	in	a,(#<(_HROW7))
	rrca
	jr	NC,00121$
;src/main.c:166: return;
	ret
00125$:
;src/main.c:168: if ((HROW6 & 0x01) == 0) //enter
	ld	a,#>(_HROW6)
	in	a,(#<(_HROW6))
	rrca
	jr	C,00130$
;src/main.c:170: keys = 0x10;
	ld	hl,#_keys + 0
	ld	(hl), #0x10
;src/main.c:171: while(!(HROW6 & 0x01));
00126$:
	ld	a,#>(_HROW6)
	in	a,(#<(_HROW6))
	rrca
	jr	NC,00126$
;src/main.c:172: return;
	ret
00130$:
;src/main.c:178: if (JOYPORT & 0x08) //up
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x08
	jr	Z,00136$
;src/main.c:180: for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
	ld	bc,#0x2710
00168$:
	dec	bc
	ld	a,b
	or	a,c
	jr	NZ,00168$
;src/main.c:181: keys = 0x08;
	ld	hl,#_keys + 0
	ld	(hl), #0x08
;src/main.c:182: while(JOYPORT & 0x08);
00132$:
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x08
	ret	Z
;src/main.c:183: return;
	jr	00132$
00136$:
;src/main.c:186: if (JOYPORT & 0x04) //down
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x04
	jr	Z,00144$
;src/main.c:188: for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
	ld	bc,#0x2710
00171$:
	dec	bc
	ld	a,b
	or	a,c
	jr	NZ,00171$
;src/main.c:189: if (JOYPORT & 0x04) 
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x04
	jr	Z,00144$
;src/main.c:191: keys = 0x04;
	ld	hl,#_keys + 0
	ld	(hl), #0x04
;src/main.c:192: while(JOYPORT & 0x04);
00138$:
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x04
	ret	Z
;src/main.c:193: return;
	jr	00138$
00144$:
;src/main.c:198: if (JOYPORT & 0x02) //left
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x02
	jr	Z,00150$
;src/main.c:200: for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
	ld	bc,#0x2710
00174$:
	dec	bc
	ld	a,b
	or	a,c
	jr	NZ,00174$
;src/main.c:201: keys = 0x02;
	ld	hl,#_keys + 0
	ld	(hl), #0x02
;src/main.c:202: while(JOYPORT & 0x02);
00146$:
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x02
	ret	Z
;src/main.c:203: return;
	jr	00146$
00150$:
;src/main.c:207: if (JOYPORT & 0x01) //right
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	rrca
	jr	NC,00156$
;src/main.c:209: for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
	ld	bc,#0x2710
00177$:
	dec	bc
	ld	a,b
	or	a,c
	jr	NZ,00177$
;src/main.c:210: keys = 0x01;
	ld	hl,#_keys + 0
	ld	(hl), #0x01
;src/main.c:211: while(JOYPORT & 0x01);
00152$:
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	rrca
	ret	NC
;src/main.c:212: return;
	jr	00152$
00156$:
;src/main.c:216: if (JOYPORT & 0x10) //action
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x10
	jp	Z,00164$
;src/main.c:218: for ( i = 0; i < DEBOUNCE_LOOP; i++ ){} //debounce
	ld	bc,#0x2710
00180$:
	dec	bc
	ld	a,b
	or	a,c
	jr	NZ,00180$
;src/main.c:219: keys = 0x10;
	ld	hl,#_keys + 0
	ld	(hl), #0x10
;src/main.c:220: while(JOYPORT & 0x10);
00158$:
	ld	a,#>(_JOYPORT)
	in	a,(#<(_JOYPORT))
	and	a, #0x10
	jr	NZ,00158$
;src/main.c:221: return;
	ret
;src/main.c:229: static void copyname( char *dst, const unsigned char *src, int l )
;	---------------------------------
; Function copyname
; ---------------------------------
_copyname:
	call	___sdcc_enter_ix
	push	af
;src/main.c:233: for( i = 0; i < l ; ++i )
	ld	c,6 (ix)
	ld	b,7 (ix)
	ld	e,4 (ix)
	ld	d,5 (ix)
	ld	hl,#0x0000
	ex	(sp), hl
00103$:
	ld	a,-2 (ix)
	sub	a, 8 (ix)
	ld	a,-1 (ix)
	sbc	a, 9 (ix)
	jp	PO, 00116$
	xor	a, #0x80
00116$:
	jp	P,00101$
;src/main.c:234: *dst++ = *src++;
	ld	a,(bc)
	inc	bc
	ld	(de),a
	inc	de
;src/main.c:233: for( i = 0; i < l ; ++i )
	inc	-2 (ix)
	jr	NZ,00103$
	inc	-1 (ix)
	jr	00103$
00101$:
;src/main.c:236: *dst++ = 0;
	xor	a, a
	ld	(de),a
	pop	af
	pop	ix
	ret
;src/main.c:239: static DIRENTRY *nthfile( int n )
;	---------------------------------
; Function nthfile
; ---------------------------------
_nthfile:
	call	___sdcc_enter_ix
	push	af
	push	af
;src/main.c:244: p = 0;
	ld	bc,#0x0000
;src/main.c:247: for( i = 0; ( j <= n ) && ( i < dir_entries ); ++i )
	ld	hl,#0x0000
	ex	(sp), hl
	ld	de,#0x0000
00106$:
	ld	a,4 (ix)
	sub	a, -4 (ix)
	ld	a,5 (ix)
	sbc	a, -3 (ix)
	jp	PO, 00127$
	xor	a, #0x80
00127$:
	jp	M,00103$
	ld	-2 (ix),e
	ld	-1 (ix),d
	ld	hl,#_dir_entries
	ld	a,-2 (ix)
	sub	a, (hl)
	ld	a,-1 (ix)
	inc	hl
	sbc	a, (hl)
	jr	NC,00103$
;src/main.c:249: p = NextDirEntry( i );
	push	de
	push	de
	call	_NextDirEntry
	pop	af
	pop	de
	ld	c, l
;src/main.c:251: if( p )
	ld	a,h
	ld	b,a
	or	a,l
	jr	Z,00107$
;src/main.c:252: ++j;
	inc	-4 (ix)
	jr	NZ,00128$
	inc	-3 (ix)
00128$:
00107$:
;src/main.c:247: for( i = 0; ( j <= n ) && ( i < dir_entries ); ++i )
	inc	de
	jr	00106$
00103$:
;src/main.c:254: return( p );
	ld	l, c
	ld	h, b
	ld	sp, ix
	pop	ix
	ret
;src/main.c:258: static void selectrom(int row)
;	---------------------------------
; Function selectrom
; ---------------------------------
_selectrom:
	call	___sdcc_enter_ix
;src/main.c:260: DIRENTRY *p=nthfile(romindex+row);
	ld	iy,#_romindex
	ld	a,0 (iy)
	add	a, 4 (ix)
	ld	c,a
	ld	a,1 (iy)
	adc	a, 5 (ix)
	ld	b,a
	push	bc
	call	_nthfile
	pop	af
	ld	c,l
	ld	b,h
;src/main.c:261: if(p)
	ld	a,b
	or	a,c
	jr	Z,00103$
;src/main.c:263: copyname(longfilename,p->Name,11);	// Make use of the long filename buffer to store a temporary copy of the filename,
	push	bc
	ld	hl,#0x000b
	push	hl
	push	bc
	ld	hl,#_longfilename
	push	hl
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	bc
;src/main.c:265: copyname(extension,p->Extension,3);
	ld	hl,#0x0008
	add	hl,bc
	ld	c,l
	ld	b,h
	ld	hl,#0x0003
	push	hl
	push	bc
	ld	hl,#_extension
	push	hl
	call	_copyname
	pop	af
	pop	af
	pop	af
00103$:
	pop	ix
	ret
;src/main.c:269: static int selectdir( int row )
;	---------------------------------
; Function selectdir
; ---------------------------------
_selectdir:
;src/main.c:271: DIRENTRY *p = nthfile( romindex + row );
	ld	hl,#2
	add	hl,sp
	ld	iy,#_romindex
	ld	a,0 (iy)
	add	a, (hl)
	ld	c,a
	ld	a,1 (iy)
	inc	hl
	adc	a, (hl)
	ld	b,a
	push	bc
	call	_nthfile
	pop	af
	ld	c,l
	ld	b,h
;src/main.c:272: if(p)
	ld	a,b
	or	a,c
	jr	Z,00104$
;src/main.c:274: if(p->Attributes&ATTR_DIRECTORY)
	ld	l, c
	ld	h, b
	ld	de, #0x000b
	add	hl, de
	ld	e,(hl)
	bit	4, e
	jr	Z,00104$
;src/main.c:276: ChangeDirectory(p);
	push	bc
	call	_ChangeDirectory
	pop	af
;src/main.c:277: romindex=0;
	ld	hl,#0x0000
	ld	(_romindex),hl
;src/main.c:278: cursor=0;
	ld	iy,#_cursor
	ld	0 (iy),#0x00
;src/main.c:279: return 1;
	ld	l, #0x01
	ret
00104$:
;src/main.c:283: return 0;
	ld	hl,#0x0000
	ret
;src/main.c:286: static void listroms()
;	---------------------------------
; Function listroms
; ---------------------------------
_listroms:
	call	___sdcc_enter_ix
	ld	hl,#-11
	add	hl,sp
	ld	sp,hl
;src/main.c:293: for( i = 0; ( j < romindex ) && ( i < dir_entries ); ++i )
	ld	bc,#0x0000
	ld	de,#0x0000
00120$:
	ld	hl,#_romindex
	ld	a,c
	sub	a, (hl)
	ld	a,b
	inc	hl
	sbc	a, (hl)
	jp	PO, 00186$
	xor	a, #0x80
00186$:
	jp	P,00103$
	ld	-9 (ix),e
	ld	-8 (ix),d
	ld	hl,#_dir_entries
	ld	a,-9 (ix)
	sub	a, (hl)
	ld	a,-8 (ix)
	inc	hl
	sbc	a, (hl)
	jr	NC,00103$
;src/main.c:295: DIRENTRY *p = NextDirEntry( i );
	push	bc
	push	de
	push	de
	call	_NextDirEntry
	pop	af
	pop	de
	pop	bc
;src/main.c:297: if( p )
	ld	a,h
	or	a,l
	jr	Z,00121$
;src/main.c:298: ++j;
	inc	bc
00121$:
;src/main.c:293: for( i = 0; ( j < romindex ) && ( i < dir_entries ); ++i )
	inc	de
	jr	00120$
00103$:
;src/main.c:301: for(j=0;(j<MAX_FILES) && (i<dir_entries);++i)
	ld	bc,#0x0000
00124$:
;src/main.c:310: romfilenames[j][0]='<'; // Right arrow
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	ld	-9 (ix),l
	ld	-8 (ix),h
;src/main.c:301: for(j=0;(j<MAX_FILES) && (i<dir_entries);++i)
	ld	a,c
	sub	a, #0x12
	ld	a,b
	rla
	ccf
	rra
	sbc	a, #0x80
	jp	NC,00143$
	ld	-2 (ix),e
	ld	-1 (ix),d
	ld	hl,#_dir_entries
	ld	a,-2 (ix)
	sub	a, (hl)
	ld	a,-1 (ix)
	inc	hl
	sbc	a, (hl)
	jp	NC,00143$
;src/main.c:303: DIRENTRY *p=NextDirEntry(i);
	push	bc
	push	de
	push	de
	call	_NextDirEntry
	pop	af
	pop	de
	pop	bc
	inc	sp
	inc	sp
	push	hl
;src/main.c:310: romfilenames[j][0]='<'; // Right arrow
	ld	a,#<(_romfilenames)
	add	a, -9 (ix)
	ld	-2 (ix),a
	ld	a,#>(_romfilenames)
	adc	a, -8 (ix)
	ld	-1 (ix),a
;src/main.c:304: if(p)
	ld	a,-10 (ix)
	or	a,-11 (ix)
	jp	Z,00114$
;src/main.c:307: if(p->Attributes&ATTR_DIRECTORY)
	pop	hl
	push	hl
	push	bc
	ld	bc, #0x000b
	add	hl, bc
	pop	bc
	ld	l,(hl)
;src/main.c:328: j++;
	inc	bc
	ld	-4 (ix),c
	ld	-3 (ix),b
;src/main.c:307: if(p->Attributes&ATTR_DIRECTORY)
	bit	4, l
	jr	Z,00111$
;src/main.c:310: romfilenames[j][0]='<'; // Right arrow
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	(hl),#0x3c
;src/main.c:311: romfilenames[j][1]='D';
	ld	a,#<(_romfilenames)
	add	a, -9 (ix)
	ld	c,a
	ld	a,#>(_romfilenames)
	adc	a, -8 (ix)
	ld	b,a
	ld	l, c
	ld	h, b
	inc	hl
	ld	(hl),#0x44
;src/main.c:312: romfilenames[j][2]='I';
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	ld	(hl),#0x49
;src/main.c:313: romfilenames[j][3]='R';
	ld	l, c
	ld	h, b
	inc	hl
	inc	hl
	inc	hl
	ld	(hl),#0x52
;src/main.c:314: romfilenames[j][4]='>';
	ld	hl,#0x0004
	add	hl,bc
	ld	(hl),#0x3e
;src/main.c:315: romfilenames[j][5]=' ';
	ld	hl,#0x0005
	add	hl,bc
	ld	(hl),#0x20
;src/main.c:317: if(longfilename[0])
	ld	hl, #_longfilename + 0
	ld	l,(hl)
;src/main.c:321: copyname(romfilenames[j]+6,longfilename,22);
	ld	a,c
	add	a, #0x06
	ld	c,a
	ld	a,b
	adc	a, #0x00
	ld	b,a
;src/main.c:317: if(longfilename[0])
	ld	a,l
	or	a, a
	jr	Z,00105$
;src/main.c:321: copyname(romfilenames[j]+6,longfilename,22);
	push	de
	ld	hl,#0x0016
	push	hl
	ld	hl,#_longfilename
	push	hl
	push	bc
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	de
	jr	00106$
00105$:
;src/main.c:325: copyname(romfilenames[j]+6,p->Name,11);
	push	de
	ld	hl,#0x000b
	push	hl
	ld	l,-11 (ix)
	ld	h,-10 (ix)
	push	hl
	push	bc
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	de
00106$:
;src/main.c:328: j++;
	ld	c,-4 (ix)
	ld	b,-3 (ix)
	jp	00125$
00111$:
;src/main.c:334: if(longfilename[0])
	ld	a,(#_longfilename + 0)
	ld	-5 (ix),a
;src/main.c:336: copyname(romfilenames[j],longfilename,28);
	ld	a,-2 (ix)
	ld	-7 (ix),a
	ld	a,-1 (ix)
	ld	-6 (ix),a
;src/main.c:334: if(longfilename[0])
	ld	a,-5 (ix)
	or	a, a
	jr	Z,00108$
;src/main.c:336: copyname(romfilenames[j],longfilename,28);
	push	de
	ld	hl,#0x001c
	push	hl
	ld	hl,#_longfilename
	push	hl
	ld	l,-7 (ix)
	ld	h,-6 (ix)
	push	hl
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	de
	jr	00109$
00108$:
;src/main.c:340: copyname(romfilenames[j],p->Name,8);
	push	de
	ld	hl,#0x0008
	push	hl
	ld	l,-11 (ix)
	ld	h,-10 (ix)
	push	hl
	ld	l,-7 (ix)
	ld	h,-6 (ix)
	push	hl
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	de
;src/main.c:341: copyname(romfilenames[j]+8,".",1);
	ld	a,-2 (ix)
	add	a, #0x08
	ld	c,a
	ld	a,-1 (ix)
	adc	a, #0x00
	ld	b,a
	push	de
	ld	hl,#0x0001
	push	hl
	ld	hl,#___str_2
	push	hl
	push	bc
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	de
;src/main.c:342: copyname(romfilenames[j]+9,p->Extension,3);
	ld	iy,#0x0008
	pop	bc
	push	bc
	add	iy, bc
	ld	a,-2 (ix)
	add	a, #0x09
	ld	l,a
	ld	a,-1 (ix)
	adc	a, #0x00
	ld	h,a
	push	de
	ld	bc,#0x0003
	push	bc
	push	iy
	push	hl
	call	_copyname
	pop	af
	pop	af
	pop	af
	pop	de
00109$:
;src/main.c:346: j++;
	ld	c,-4 (ix)
	ld	b,-3 (ix)
	jr	00125$
00114$:
;src/main.c:351: romfilenames[j][0]=0;
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	(hl),#0x00
00125$:
;src/main.c:301: for(j=0;(j<MAX_FILES) && (i<dir_entries);++i)
	inc	de
	jp	00124$
00143$:
	ld	e,-9 (ix)
	ld	d,-8 (ix)
00127$:
;src/main.c:353: for(;j<MAX_FILES;++j)
	ld	a,c
	sub	a, #0x12
	ld	a,b
	rla
	ccf
	rra
	sbc	a, #0x80
	jr	NC,00129$
;src/main.c:354: romfilenames[j][0]=0;
	ld	hl,#_romfilenames
	add	hl,de
	ld	(hl),#0x00
;src/main.c:353: for(;j<MAX_FILES;++j)
	ld	hl,#0x001e
	add	hl,de
	ex	de,hl
	inc	bc
	jr	00127$
00129$:
	ld	sp, ix
	pop	ix
	ret
___str_2:
	.ascii "."
	.db 0x00
;src/main.c:356: void draw_menu()
;	---------------------------------
; Function draw_menu
; ---------------------------------
_draw_menu::
;src/main.c:360: cursor = 0;
	ld	hl,#_cursor + 0
	ld	(hl), #0x00
;src/main.c:362: vdp_cls();
	call	_vdp_cls
;src/main.c:364: vdp_setcolor(COLOR_BLUE, COLOR_RED, COLOR_WHITE);
	ld	hl,#0x0f02
	push	hl
	ld	a,#0x01
	push	af
	inc	sp
	call	_vdp_setcolor
;src/main.c:365: vdp_prints("           SD LOADER            ");
	inc	sp
	ld	hl,#___str_3
	ex	(sp),hl
	call	_vdp_prints
	pop	af
;src/main.c:367: vdp_setbg(COLOR_BLACK);	
	xor	a, a
	push	af
	inc	sp
	call	_vdp_setbg
	inc	sp
;src/main.c:368: vdp_prints("  Up/Down - Select L/R - Page   ");
	ld	hl,#___str_4
	push	hl
	call	_vdp_prints
;src/main.c:369: vdp_prints("     Action Button to load      ");
	ld	hl, #___str_5
	ex	(sp),hl
	call	_vdp_prints
	pop	af
;src/main.c:371: listroms();
	call	_listroms
;src/main.c:375: for( i = 0; i < MAX_FILES; ++i )
	ld	c,#0x00
	ld	de,#0x0000
00102$:
;src/main.c:377: vdp_gotoxy( 3, i + 4 );
	ld	a,c
	add	a, #0x04
	push	bc
	push	de
	ld	d,a
	ld	e,#0x03
	push	de
	call	_vdp_gotoxy
	pop	af
	pop	de
	pop	bc
;src/main.c:378: vdp_prints( romfilenames[ i ] );
	ld	hl,#_romfilenames
	add	hl,de
	push	bc
	push	de
	push	hl
	call	_vdp_prints
	ld	hl, #___str_6
	ex	(sp),hl
	call	_vdp_prints
	pop	af
	pop	de
	pop	bc
;src/main.c:375: for( i = 0; i < MAX_FILES; ++i )
	ld	hl,#0x001e
	add	hl,de
	ex	de,hl
	inc	c
	ld	a,c
	sub	a, #0x12
	jr	C,00102$
;src/main.c:384: draw_cursor();
	jp  _draw_cursor
___str_3:
	.ascii "           SD LOADER            "
	.db 0x00
___str_4:
	.ascii "  Up/Down - Select L/R - Page   "
	.db 0x00
___str_5:
	.ascii "     Action Button to load      "
	.db 0x00
___str_6:
	.ascii " "
	.db 0x00
;src/main.c:387: void main()
;	---------------------------------
; Function main
; ---------------------------------
_main::
	call	___sdcc_enter_ix
	ld	hl,#-28
	add	hl,sp
	ld	sp,hl
;src/main.c:405: vdp_init();
	call	_vdp_init
;src/main.c:408: REG_NUM = 0; //inital state
	ld	a,#0x00
	ld	bc,#_REG_NUM
	out	(c),a
;src/main.c:411: if (!MMC_Init()) 
	call	_MMC_Init
	ld	a,l
	or	a, a
	jr	NZ,00105$
;src/main.c:414: display_error("Error initializing SD card!");
	ld	hl,#___str_7
	push	hl
	call	_display_error
	pop	af
;src/main.c:415: while(1){}
00102$:
	jr	00102$
00105$:
;src/main.c:418: if (!FindDrive()) 
	call	_FindDrive
	ld	a,h
	or	a,l
	jr	NZ,00245$
;src/main.c:421: display_error("  Error mounting SD card!");
	ld	hl,#___str_8
	push	hl
	call	_display_error
	pop	af
;src/main.c:422: while(1){}
00107$:
	jr	00107$
;src/main.c:425: while( 1 ) 
00245$:
	ld	de,#_romfilenames+0
	ld	hl,#0x0000
	add	hl,sp
	ld	-2 (ix),l
	ld	-1 (ix),h
00147$:
;src/main.c:427: keys = 0;
	ld	hl,#_keys + 0
	ld	(hl), #0x00
;src/main.c:428: romindex = 0;
	ld	hl,#0x0000
	ld	(_romindex),hl
;src/main.c:430: while( 1 ) 
00137$:
;src/main.c:433: draw_menu();
	push	de
	call	_draw_menu
	pop	de
;src/main.c:436: while( 1 ) 
00132$:
;src/main.c:439: readkeyb();
	push	de
	call	_readkeyb
	pop	de
;src/main.c:441: if ( keys == 0x08 ) //up
	ld	a,(#_keys + 0)
	sub	a, #0x08
	jr	NZ,00129$
;src/main.c:443: hide_cursor();
	push	de
	call	_hide_cursor
	pop	de
;src/main.c:445: if ( cursor > 0 ) cursor--;
	ld	iy,#_cursor
	ld	a,0 (iy)
	or	a, a
	jr	Z,00112$
	dec	0 (iy)
00112$:
;src/main.c:447: draw_cursor();
	push	de
	call	_draw_cursor
	pop	de
	jr	00132$
00129$:
;src/main.c:450: else if ( keys == 0x04 ) //down
	ld	a,(#_keys + 0)
	sub	a, #0x04
	jr	NZ,00126$
;src/main.c:452: hide_cursor();
	push	de
	call	_hide_cursor
	pop	de
;src/main.c:454: if ( romfilenames[ cursor + 1 ][ 0 ] != 0 ) cursor++;
	ld	iy,#_cursor
	ld	c,0 (iy)
	ld	b,#0x00
	inc	bc
	ld	l, c
	ld	h, b
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl, bc
	add	hl, hl
	add	hl,de
	ld	a,(hl)
	or	a, a
	jr	Z,00114$
	inc	0 (iy)
00114$:
;src/main.c:456: draw_cursor();
	push	de
	call	_draw_cursor
	pop	de
	jr	00132$
00126$:
;src/main.c:459: else if ( keys == 0x02 ) //left
	ld	a,(#_keys + 0)
	sub	a, #0x02
	jr	NZ,00123$
;src/main.c:461: if ( romindex > 0 ) 
	xor	a, a
	ld	iy,#_romindex
	cp	a, 0 (iy)
	sbc	a, 1 (iy)
	jp	PO, 00478$
	xor	a, #0x80
00478$:
	jp	P,00132$
;src/main.c:463: romindex -= MAX_FILES;
	ld	hl,#_romindex
	ld	a,(hl)
	add	a,#0xee
	ld	(hl),a
	inc	hl
	ld	a,(hl)
	adc	a,#0xff
	ld	(hl),a
;src/main.c:464: draw_menu();
	push	de
	call	_draw_menu
	pop	de
	jp	00132$
00123$:
;src/main.c:467: else if ( keys == 0x01 ) //right
	ld	a,(#_keys + 0)
	dec	a
	jr	NZ,00120$
;src/main.c:471: romindex += MAX_FILES;
	ld	hl,#_romindex
	ld	a,(hl)
	add	a, #0x12
	ld	(hl),a
	inc	hl
	ld	a,(hl)
	adc	a, #0x00
	ld	(hl),a
;src/main.c:472: draw_menu();
	push	de
	call	_draw_menu
	pop	de
	jp	00132$
00120$:
;src/main.c:475: else if ( keys == 0x10 ) // action button 
	ld	a,(#_keys + 0)
	sub	a, #0x10
	jp	NZ,00132$
;src/main.c:481: if (selectdir ( cursor ) != 1) // try to open as folder
	ld	hl,#_cursor + 0
	ld	c, (hl)
	ld	b,#0x00
	push	de
	push	bc
	call	_selectdir
	pop	af
	pop	de
	dec	l
	jr	NZ,00483$
	ld	a,h
	or	a, a
	jp	Z,00137$
00483$:
;src/main.c:483: selectrom( cursor ); //open as file
	ld	hl,#_cursor + 0
	ld	c, (hl)
	ld	b,#0x00
	push	de
	push	bc
	call	_selectrom
	pop	af
	call	_vdp_cls
	ld	hl,#0x0c00
	push	hl
	call	_vdp_gotoxy
	ld	hl, #___str_9
	ex	(sp),hl
	call	_vdp_prints
	pop	af
	pop	de
;src/main.c:493: if (!FileOpen(&file, longfilename)) {
	ld	hl,#_longfilename
	ld	c,-2 (ix)
	ld	b,-1 (ix)
	push	de
	push	hl
	push	bc
	call	_FileOpen
	pop	af
	pop	af
	pop	de
	ld	a,h
	or	a,l
	jr	NZ,00140$
;src/main.c:495: display_error("    Error opening the file     ");
	push	de
	ld	hl,#___str_10
	push	hl
	call	_display_error
	pop	af
	pop	de
;src/main.c:496: continue;
	jp	00147$
00140$:
;src/main.c:499: repeat = 0;
	ld	-6 (ix),#0x00
	ld	-5 (ix),#0x00
;src/main.c:500: bs_method = file.size / 512;
	ld	l,-2 (ix)
	ld	h,-1 (ix)
	ld	de, #0x0004
	add	hl, de
	ld	c,(hl)
	inc	hl
	ld	b,(hl)
	inc	hl
	ld	e,(hl)
	inc	hl
	ld	d,(hl)
	ld	a,#0x09
00484$:
	srl	d
	rr	e
	rr	b
	rr	c
	dec	a
	jr	NZ,00484$
;src/main.c:501: blocks = bs_method;
	ld	-12 (ix),c
	ld	-11 (ix),#0x00
;src/main.c:517: while (c < blocks) {
	ld	-10 (ix),#0x00
	ld	-9 (ix),#0x00
	ld	-8 (ix),#0x00
	ld	-7 (ix),#0x80
00143$:
	ld	a,-10 (ix)
	sub	a, -12 (ix)
	ld	a,-9 (ix)
	sbc	a, -11 (ix)
	jp	PO, 00486$
	xor	a, #0x80
00486$:
	jp	P,00289$
;src/main.c:518: if (!FileRead(&file, mem)) {
	ld	c,-2 (ix)
	ld	b,-1 (ix)
	ld	l,-8 (ix)
	ld	h,-7 (ix)
	push	hl
	push	bc
	call	_FileRead
	pop	af
	pop	af
	ld	-3 (ix),h
	ld	-4 (ix),l
	ld	a,-3 (ix)
	or	a,-4 (ix)
	jr	NZ,00142$
;src/main.c:520: display_error("     Error reading the file     ");
	ld	hl,#___str_11
	push	hl
	call	_display_error
	pop	af
;src/main.c:521: continue;
	jr	00143$
00142$:
;src/main.c:523: c++;
	inc	-10 (ix)
	jr	NZ,00487$
	inc	-9 (ix)
00487$:
;src/main.c:524: mem += 512;
	ld	a,-8 (ix)
	add	a, #0x00
	ld	-8 (ix),a
	ld	a,-7 (ix)
	adc	a, #0x02
	ld	-7 (ix),a
	jr	00143$
;src/main.c:527: break;
00289$:
	ld	a,-8 (ix)
	ld	-4 (ix),a
	ld	a,-7 (ix)
	ld	-3 (ix),a
;src/main.c:530: DisableCard();
	ld	a,#0xff
	out	(_SD_CONTROL),a
;src/main.c:534: if ( blocks == 4) //2kb
	ld	a,-12 (ix)
	sub	a, #0x04
	jr	NZ,00150$
	ld	a,-11 (ix)
	or	a, a
	jr	NZ,00150$
;src/main.c:536: repeat = 3;
	ld	-6 (ix),#0x03
	ld	-5 (ix),#0x00
00150$:
;src/main.c:538: if ( blocks == 8) //4kb
	ld	a,-12 (ix)
	sub	a, #0x08
	jr	NZ,00152$
	ld	a,-11 (ix)
	or	a, a
	jr	NZ,00152$
;src/main.c:540: repeat = 2;
	ld	-6 (ix),#0x02
	ld	-5 (ix),#0x00
00152$:
;src/main.c:543: addr = 0;
	ld	de,#0x0000
;src/main.c:545: for ( i = 0 ; i < repeat; ++i )
	ld	a,-12 (ix)
	add	a, a
	ld	b,a
	ld	c,#0x00
	ld	-14 (ix),#0x00
	ld	-13 (ix),#0x00
00225$:
	ld	a,-14 (ix)
	sub	a, -6 (ix)
	ld	a,-13 (ix)
	sbc	a, -5 (ix)
	jp	PO, 00492$
	xor	a, #0x80
00492$:
	jp	P,00154$
;src/main.c:547: for ( j = 0 ; j < blocks * 512; ++j )
	ld	-16 (ix),#0x00
	ld	-15 (ix),#0x00
00222$:
	ld	a,-16 (ix)
	sub	a, c
	ld	a,-15 (ix)
	sbc	a, b
	jp	PO, 00493$
	xor	a, #0x80
00493$:
	jp	P,00290$
;src/main.c:549: mem[j] = mem_ini[addr];
	push	hl
	ld	l,-4 (ix)
	ld	h,-3 (ix)
	push	hl
	pop	iy
	pop	hl
	push	bc
	ld	c,-16 (ix)
	ld	b,-15 (ix)
	add	iy, bc
	pop	bc
	ld	hl,#0x8000
	add	hl,de
	ld	l,(hl)
	ld	0 (iy), l
;src/main.c:550: addr++;
	inc	de
;src/main.c:547: for ( j = 0 ; j < blocks * 512; ++j )
	inc	-16 (ix)
	jr	NZ,00222$
	inc	-15 (ix)
	jr	00222$
00290$:
;src/main.c:553: mem += blocks * 512;	
	ld	a,-4 (ix)
	add	a, c
	ld	-4 (ix),a
	ld	a,-3 (ix)
	adc	a, b
	ld	-3 (ix),a
;src/main.c:545: for ( i = 0 ; i < repeat; ++i )
	inc	-14 (ix)
	jr	NZ,00225$
	inc	-13 (ix)
	jr	00225$
00154$:
;src/main.c:578: ULAPORT = COLOR_GREEN;
	ld	a,#0x04
	out	(_ULAPORT),a
;src/main.c:607: if (extension[0]=='F' && extension[1]=='4'&& extension[2]=='S')
	ld	hl,#_extension+0
	ld	a,(hl)
	ld	-4 (ix), a
	sub	a, #0x46
	jr	NZ,00496$
	ld	a,#0x01
	jr	00497$
00496$:
	xor	a,a
00497$:
	ld	c,a
	ex	de,hl
	inc	de
	ld	a,c
	or	a, a
	jr	Z,00214$
	ld	a,(de)
	sub	a, #0x34
	jr	NZ,00214$
	ld	a, (#(_extension + 0x0002) + 0)
	sub	a, #0x53
	jr	NZ,00214$
;src/main.c:609: bs_method = BANKF4_SC;
	ld	a,#0x16
	jp	00215$
00214$:
;src/main.c:611: else if (extension[0]=='F' && extension[1]=='4')
	ld	a,c
	or	a, a
	jr	Z,00210$
	ld	a,(de)
	sub	a, #0x34
	jr	NZ,00210$
;src/main.c:613: bs_method = BANKF4;
	ld	a,#0x06
	jp	00215$
00210$:
;src/main.c:615: else if (extension[0]=='F' && extension[1]=='6' && extension[2]=='S')
	ld	a,c
	or	a, a
	jr	Z,00205$
	ld	a,(de)
	sub	a, #0x36
	jr	NZ,00205$
	ld	a, (#(_extension + 0x0002) + 0)
	sub	a, #0x53
	jr	NZ,00205$
;src/main.c:617: bs_method = BANKF6_SC;
	ld	a,#0x12
	jp	00215$
00205$:
;src/main.c:619: else if (extension[0]=='F' && extension[1]=='6')
	ld	a,c
	or	a, a
	jr	Z,00201$
	ld	a,(de)
	sub	a, #0x36
	jr	NZ,00201$
;src/main.c:621: bs_method = BANKF6;
	ld	a,#0x02
	jp	00215$
00201$:
;src/main.c:623: else if (extension[0]=='F' && extension[1]=='8' && extension[2]=='S')
	ld	a,c
	or	a, a
	jr	Z,00196$
	ld	a,(de)
	sub	a, #0x38
	jr	NZ,00196$
	ld	a, (#(_extension + 0x0002) + 0)
	sub	a, #0x53
	jr	NZ,00196$
;src/main.c:625: bs_method = BANKF8_SC;
	ld	a,#0x11
	jp	00215$
00196$:
;src/main.c:627: else if (extension[0]=='F' && extension[1]=='8')
	ld	a,c
	or	a, a
	jr	Z,00192$
	ld	a,(de)
	sub	a, #0x38
	jr	NZ,00192$
;src/main.c:629: bs_method = BANKF8;
	ld	a,#0x01
	jp	00215$
00192$:
;src/main.c:631: else if (extension[0]=='F' && extension[1]=='E')
	ld	a,c
	or	a, a
	jr	Z,00188$
	ld	a,(de)
	sub	a, #0x45
	jr	NZ,00188$
;src/main.c:633: bs_method = BANKFE;
	ld	a,#0x03
	jp	00215$
00188$:
;src/main.c:635: else if (extension[0]=='E' && extension[1]=='0')
	ld	a,-4 (ix)
	sub	a, #0x45
	jr	NZ,00518$
	ld	a,#0x01
	jr	00519$
00518$:
	xor	a,a
00519$:
	ld	-2 (ix), a
	or	a, a
	jr	Z,00184$
	ld	a,(de)
	sub	a, #0x30
	jr	NZ,00184$
;src/main.c:637: bs_method = BANKE0;
	ld	a,#0x04
	jp	00215$
00184$:
;src/main.c:639: else if (extension[0]=='3' && extension[1]=='F')
	ld	a,-4 (ix)
	sub	a, #0x33
	jr	NZ,00180$
	ld	a,(de)
	sub	a, #0x46
	jr	NZ,00180$
;src/main.c:641: bs_method = BANK3F;
	ld	a,#0x05
	jr	00215$
00180$:
;src/main.c:645: else if (extension[0]=='D' && extension[1]=='P' && extension[2]=='C')
	ld	a,-4 (ix)
	sub	a, #0x44
	jr	NZ,00175$
	ld	a,(de)
	sub	a, #0x50
	jr	NZ,00175$
	ld	a, (#(_extension + 0x0002) + 0)
	sub	a, #0x43
	jr	NZ,00175$
;src/main.c:647: bs_method = BANKP2;
	ld	a,#0x07
	jr	00215$
00175$:
;src/main.c:649: else if (extension[0]=='F' && extension[1]=='A')
	ld	a,c
	or	a, a
	jr	Z,00171$
	ld	a,(de)
	sub	a, #0x41
	jr	NZ,00171$
;src/main.c:651: bs_method = BANKFA;
	ld	a,#0x08
	jr	00215$
00171$:
;src/main.c:653: else if (extension[0]=='C' && extension[1]=='V')
	ld	a,-4 (ix)
	sub	a, #0x43
	jr	NZ,00167$
	ld	a,(de)
	sub	a, #0x56
	jr	NZ,00167$
;src/main.c:655: bs_method = BANKCV;
	ld	a,#0x09
	jr	00215$
00167$:
;src/main.c:657: else if (extension[0]=='E' && extension[1]=='7')
	ld	a,-2 (ix)
	or	a, a
	jr	Z,00163$
	ld	a,(de)
	sub	a, #0x37
	jr	NZ,00163$
;src/main.c:659: bs_method = BANKE7;
	ld	a,#0x0a
	jr	00215$
00163$:
;src/main.c:661: else if (extension[0]=='U' && extension[1]=='A')
	ld	a,-4 (ix)
	sub	a, #0x55
	jr	NZ,00159$
	ld	a,(de)
	sub	a, #0x41
	jr	NZ,00159$
;src/main.c:663: bs_method = BANKUA;
	ld	a,#0x0b
	jr	00215$
00159$:
;src/main.c:668: if ( blocks == 16) //se tem 8kb, sem extensao, supomos que é um F8
	ld	a,-12 (ix)
	sub	a, #0x10
	jr	NZ,00156$
	ld	a,-11 (ix)
	or	a, a
	jr	NZ,00156$
;src/main.c:670: bs_method = BANKF8;
	ld	a,#0x01
	jr	00215$
00156$:
;src/main.c:674: bs_method = 0;
	ld	a,#0x00
00215$:
;src/main.c:680: bs_method = bs_method | 0x80;
	set	7, a
	ld	bc,#_REG_NUM
	out	(c),a
;src/main.c:689: while(1){}
00219$:
	jr	00219$
___str_7:
	.ascii "Error initializing SD card!"
	.db 0x00
___str_8:
	.ascii "  Error mounting SD card!"
	.db 0x00
___str_9:
	.ascii "          Loading...            "
	.db 0x00
___str_10:
	.ascii "    Error opening the file     "
	.db 0x00
___str_11:
	.ascii "     Error reading the file     "
	.db 0x00
	.area _CODE
	.area _INITIALIZER
	.area _CABS (ABS)
