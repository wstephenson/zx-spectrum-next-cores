//---------------------------------------------------------------------------------
//-- Arcade Ports to ZX-UNO by Neuro
//-- Based on the code of Darfpga
//---------------------------------------------------------------------------------


//-------------------------------------------------------------------------------
//--
//-- ZX Spectrum Next top by Victor Trucco - 2020
//--
//-------------------------------------------------------------------------------


`timescale 1 ps / 1 ps

`default_nettype none


module ckong_top (
	// Clocks
      input  wire clock_50_i,

      //SRAM (AS7C34096)
      output wire [18:0] ram_addr_o,
      inout  wire [15:0] ram_data_io,
      output wire ram_oe_n_o,
      output wire ram_we_n_o,
      output wire [3:0] ram_ce_n_o,

     // PS2
      inout wire ps2_clk_io,
      inout wire ps2_data_io,
      inout wire ps2_pin6_io,
      inout wire ps2_pin2_io,
                            
      // SD Card
      output wire sd_cs0_n_o,
      output wire sd_cs1_n_o,
      output wire sd_sclk_o,
      output wire sd_mosi_o,
      input  wire sd_miso_i,

      // Flash
      output wire flash_cs_n_o,
      output wire flash_sclk_o,
      output wire flash_mosi_o,
      input  wire flash_miso_i,
      output wire flash_wp_o,
      output wire flash_hold_o,

      // Joystick
      input  wire joyp1_i,
      input  wire joyp2_i,
      input  wire joyp3_i,
      input  wire joyp4_i,
      input  wire joyp6_i,
      output wire joyp7_o,
      input  wire joyp9_i,
      output wire joysel_o,

      // Audio
      output wire audioext_l_o,
      output wire audioext_r_o,
      output wire audioint_o,

      // K7
      output wire ear_port_i,
      input  wire mic_port_o,

      // Buttons
      input  wire btn_divmmc_n_i,
      input  wire btn_multiface_n_i,
      input  wire btn_reset_n_i,

      // Matrix keyboard
      output wire [7:0] keyb_row_o,
      input  wire [6:0] keyb_col_i,

      // Bus
      inout  wire bus_rst_n_io,
      output wire bus_clk35_o,
      output wire [15:0] bus_addr_o,
      inout  wire [7:0] bus_data_io,
      inout  wire bus_int_n_io,
      input  wire bus_nmi_n_i,
      input  wire bus_ramcs_i,
      input  wire bus_romcs_i,
      input  wire bus_wait_n_i,
      output wire bus_halt_n_o,
      output wire bus_iorq_n_o,
      output wire bus_m1_n_o,
      output wire bus_mreq_n_o,
      output wire bus_rd_n_o,
      output wire bus_wr_n_o,
      output wire bus_rfsh_n_o,
      input  wire bus_busreq_n_i,
      output wire bus_busack_n_o,
      input  wire bus_iorqula_n_i,

      // VGA
      output wire [2:0] rgb_r_o,
      output wire [2:0] rgb_g_o,
      output wire [2:0] rgb_b_o,
      output wire hsync_o,
      output wire vsync_o,
      output wire csync_o,

      // HDMI
      output wire [3:0] hdmi_p_o,
      output wire [3:0] hdmi_n_o,

      // I2C (RTC and HDMI)
      inout  wire i2c_scl_io,
      inout  wire i2c_sda_io,

      // ESP
      inout  wire esp_gpio0_io,
      inout  wire esp_gpio2_io,
      input  wire esp_rx_i,
      output wire esp_tx_o,

      // PI GPIO
      inout  wire [27:0] accel_io,

      // Vacant pins
      inout  wire extras_io
);
	wire pclk;

  reg joysel_s = 0;
	reg [5:0] joy1_s;
	reg [5:0] joy2_s;
	
	always @(posedge pclk)
	begin
			joysel_s <= ~joysel_s;
			
			if (joysel_s == 0)
				joy1_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
			else
			   joy2_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
	end 
	
	assign joysel_o = joysel_s;
	

  reg [1:0] scandblctrl = 2'b01;
  wire [8:0] membrane_joy_s;

  wire pllclk0, pllclk1, pllclk2;
  wire pclkx2, pclkx10, pll_lckd;
  wire clkfbout;
  wire reset;
  
 // wire [5:0] JOYSTICK2;          //Variable temporal hasta que se meta la se�al de Joystick 2 en el .UCF
  //assign JOYSTICK2 = 6'b111111;  //Asignacion temporal al Joystick en Pullup
  //assign JOYSTICK2 = JOYSTICK;

  BUFG pclkbufg (.I(pllclk1), .O(pclk));

  //////////////////////////////////////////////////////////////////
  // 2x pclk is going to be used to drive OSERDES2
  // on the GCLK side
  //////////////////////////////////////////////////////////////////
  BUFG pclkx2bufg (.I(pllclk2), .O(pclkx2));

  //////////////////////////////////////////////////////////////////
  // 10x pclk is used to drive IOCLK network so a bit rate reference
  // can be used by OSERDES2
  //////////////////////////////////////////////////////////////////
  PLL_BASE # (
    .CLKIN_PERIOD(20),
    .CLKFBOUT_MULT(20),  //10 //set VCO to 10x of CLKIN
    .CLKOUT0_DIVIDE(4),  //2
    .CLKOUT1_DIVIDE(41), //20
    .CLKOUT2_DIVIDE(20), //10
    .COMPENSATION("INTERNAL")
  ) PLL_OSERDES (
    .CLKFBOUT(clkfbout),
    .CLKOUT0(pllclk0),
    .CLKOUT1(pllclk1),
    .CLKOUT2(pllclk2),
    .CLKOUT3(),
    .CLKOUT4(),
    .CLKOUT5(),
    .LOCKED(pll_lckd),
    .CLKFBIN(clkfbout),
    .CLKIN(clock_50_i),
    .RST(1'b0)
  );

  wire serdesstrobe;
  wire bufpll_lock;
  BUFPLL #(.DIVIDE(5)) ioclk_buf (.PLLIN(pllclk0), .GCLK(pclkx2), .LOCKED(pll_lckd),
           .IOCLK(pclkx10), .SERDESSTROBE(serdesstrobe), .LOCK(bufpll_lock));

  synchro #(.INITIALIZE("LOGIC1"))
  synchro_reset (.async(!pll_lckd),.sync(reset),.clk(pclk));
  

  assign ram_addr_o[18:17] = 2'b00;
  
  // MODULO
  reg [7:0] delay_count;
  reg pm_reset;
  wire ena_12;
  wire ena_6;
  
  always @ (posedge pclk or negedge pll_lckd) begin
    if (!pll_lckd) begin
      delay_count <= 8'd0;
      pm_reset <= 1'b1;
    end else begin
      delay_count <= delay_count + 1'b1;
      if (delay_count == 8'hff)
        pm_reset <= 1'b0;        
    end
  end
    
  assign ena_12 = delay_count[0];
  assign ena_6 = delay_count[0] & ~delay_count[1];

  wire resetKey, master_reset, resetHW;
  wire [20:0]scanSW;
 
 assign resetHW = resetKey | ~btn_reset_n_i; 
 
wire ext_rst;

reg [2:0]M_VIDEO_R, M_VIDEO_G, M_VIDEO_B;
reg M_HSYNC,M_VSYNC;
assign rgb_r_o = M_VIDEO_R;
assign rgb_g_o = M_VIDEO_G;
assign rgb_b_o = M_VIDEO_B;
assign hsync_o=M_HSYNC;
assign vsync_o=M_VSYNC;


reg O_AUDIO_ML,O_AUDIO_MR;

assign audioext_l_o = O_AUDIO_ML;
assign audioext_r_o = O_AUDIO_ML;
  
  wire [7:0] joystick1, joystick2;
   reg [7:0] joy1, joy2;
   reg joy_split;

 
  ckong pm (
	 .clock_12(ena_12),
	 .reset(resetHW),
	 .tv15Khz_mode(!scandblctrl[0]),
    .video_r(M_VIDEO_R),
    .video_g(M_VIDEO_G),
    .video_b(M_VIDEO_B),
    .video_hs(M_HSYNC),
    .video_vs(M_VSYNC),

    .audio_out_l(O_AUDIO_ML),
    .audio_out_r(O_AUDIO_MR),
    .I_JOYSTICK_A(joy1_s),
    .I_JOYSTICK_B(joy2_s),
		.membrane_joy(membrane_joy_s),
  
    .JOYSTICK_A_GND(),
    .JOYSTICK_B_GND(),
	 .I_PLAYER(2'b11),
	 .I_COIN(1'b1),
	 .scanSW(scanSW),

	 .scandblctrl(scandblctrl)
  );


 // 0x8FD5 SRAM (SCANDBLCTRL ZXUNO REG)  
 assign ram_addr_o = 21'b000001000111111010101; 	
//assign scandblctrl = sram_dq[1:0];  
 assign ram_we_n_o = 1'b1;
 assign ram_ce_n_o = 4'b1111;

  keyboard keyb (
		.CLOCK(pclk),
		.PS2_CLK(ps2_clk_io),
		.PS2_DATA(ps2_data_io),
		.resetKey(resetKey),
		.MRESET(master_reset),
		.scanSW(scanSW)
	);
  
//-----------------Multiboot-------------
//	multiboot el_multiboot (
//	  .clk_icap(pclk),
//	  .REBOOT(master_reset || !ext_rst)
//	);  

//------------------------------------------------------------------
	//-- membrane keyboard
	//------------------------------------------------------------------

	wire btn_scandb_s;
 debounce_nmi
  #
  (
    .counter_size  (3)
  )
	debounce_nmi
  (
    .clk_i     ( pclk ),
    .button_i  ( ~btn_multiface_n_i & membrane_joy_s[8]),
    .result_o  ( btn_scandb_s )   
	);
	
	always @(posedge btn_scandb_s, posedge reset)
	begin
			if (reset) 
				scandblctrl = 2'b01;
			else
				scandblctrl[0] = ~scandblctrl[0];
	end

	 membrane_joystick membrane_joystick 
	 (
	
      .clock       		( pclk ),
      .reset       		( reset ),

      .membrane_joy_o  	( membrane_joy_s), //-- P2, P1, COIN, F2, F1, R, L, D, U
		
      .keyb_row_o   		( keyb_row_o ),   
      .i_membrane_cols  ( keyb_col_i )
  
	);
	
	
	//--------------------------------------------------------
	//-- Unused outputs
	//--------------------------------------------------------

	
	 // TODO: add support for HDMI output
   OBUFDS OBUFDS_c0  ( .O  ( hdmi_p_o[0]), .OB  ( hdmi_n_o[0]), .I (1'b1));
   OBUFDS OBUFDS_c1  ( .O  ( hdmi_p_o[1]), .OB  ( hdmi_n_o[1]), .I (1'b1));
   OBUFDS OBUFDS_c2  ( .O  ( hdmi_p_o[2]), .OB  ( hdmi_n_o[2]), .I (1'b1));
   OBUFDS OBUFDS_clk ( .O  ( hdmi_p_o[3]), .OB  ( hdmi_n_o[3]), .I (1'b1));
   
   // -- Interal audio (speaker, not fitted)
    assign audioint_o     = 1'b0;

    //-- Spectrum Next Bus
    assign bus_addr_o     = 16'hFFFF;
    assign bus_busack_n_o = 1'b1;
    assign bus_clk35_o    = 1'b1;
    assign bus_data_io    = 8'hFF;
    assign bus_halt_n_o   = 1'b1;
    assign bus_iorq_n_o   = 1'b1;
    assign bus_m1_n_o     = 1'b1;
    assign bus_mreq_n_o   = 1'b1;
    assign bus_rd_n_o     = 1'b1;
    assign bus_rfsh_n_o   = 1'b1;
    assign bus_rst_n_io   = 1'b1;
    assign bus_wr_n_o     = 1'b1;

    //-- ESP 8266 module
    assign esp_gpio0_io   = 1'bZ;
    assign esp_gpio2_io   = 1'bZ;
    assign esp_tx_o = 1'b1;
	 
    //-- Addtional flash pins; used at IO2 and IO3 in Quad SPI Mode
    assign flash_hold_o   = 1'b1;
    assign flash_wp_o     = 1'b1;
	 
	 assign flash_cs_n_o  = 1'b1;
    assign flash_sclk_o  = 1'b1;
    assign flash_mosi_o  = 1'b1;

    assign ear_port_i = 1'b1;
		
	 assign i2c_scl_io = 1'bZ;
    assign i2c_sda_io = 1'bZ;

    //-- Pin 7 on the joystick connecter. 
    assign joyp7_o    = 1'b1;


    //-- Mic Port (output, as it connects to the mic input on cassette deck)
    assign mic_port_o = 1'b0;

	 //-- CS2 is for internal SD socket
    assign sd_cs1_n_o = 1'b1;
	 
    // PI GPIO
    assign accel_io = 28'bZZZZZZZZZZZZZZZZZZZZZZZZZZZZ;

    // Vacant pins
    assign extras_io = 1'b1;

	 assign csync_o = 1'b1;
		 
	 assign sd_cs0_n_o = 1'b1;
    assign sd_cs1_n_o = 1'b1;
    assign sd_sclk_o  = 1'b1;
    assign sd_mosi_o  = 1'b1;

    assign ram_oe_n_o = 1'b1;
	 assign ram_we_n_o = 1'b1;
	  
	  
endmodule
