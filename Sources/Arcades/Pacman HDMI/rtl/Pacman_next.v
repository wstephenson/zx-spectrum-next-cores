//============================================================================
//  Arcade: Pacman
//
//  Top with full framebuffer and HDMI
//  Copyright (C) 2020 Victor Trucco
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  ZX Spectrum Next Top by Victor Trucco 2020
//
//============================================================================

`default_nettype none

module Pacman
(
    // Clocks
  input  wire clock_50_i,

  //SRAM (AS7C34096)
  output reg [18:0] ram_addr_o,
  inout  wire [15:0] ram_data_io,
  output wire ram_oe_n_o,
  output reg ram_we_n_o,
  output wire [3:0] ram_ce_n_o,

 // PS2
  inout wire ps2_clk_io,
  inout wire ps2_data_io,
  inout wire ps2_pin6_io,
  inout wire ps2_pin2_io,
                        
  // SD Card
  output wire sd_cs0_n_o,
  output wire sd_cs1_n_o,
  output wire sd_sclk_o,
  output wire sd_mosi_o,
  input  wire sd_miso_i,

  // Flash
  output wire flash_cs_n_o,
  output wire flash_sclk_o,
  output wire flash_mosi_o,
  input  wire flash_miso_i,
  output wire flash_wp_o,
  output wire flash_hold_o,

  // Joystick
  input  wire joyp1_i,
  input  wire joyp2_i,
  input  wire joyp3_i,
  input  wire joyp4_i,
  input  wire joyp6_i,
  output wire joyp7_o,
  input  wire joyp9_i,
  output wire joysel_o,

  // Audio
  output wire audioext_l_o,
  output wire audioext_r_o,
  output wire audioint_o,

  // K7
  input  wire ear_port_i,
  output wire mic_port_o,

  // Buttons
  input  wire btn_divmmc_n_i,
  input  wire btn_multiface_n_i,
  input  wire btn_reset_n_i,

  // Matrix keyboard
  output wire [7:0] keyb_row_o,
  input  wire [6:0] keyb_col_i,

  // Bus
  inout  wire bus_rst_n_io,
  output wire bus_clk35_o,
  output wire [15:0] bus_addr_o,
  inout  wire [7:0] bus_data_io,
  inout  wire bus_int_n_io,
  input  wire bus_nmi_n_i,
  input  wire bus_ramcs_i,
  input  wire bus_romcs_i,
  input  wire bus_wait_n_i,
  output wire bus_halt_n_o,
  output wire bus_iorq_n_o,
  output wire bus_m1_n_o,
  output wire bus_mreq_n_o,
  output wire bus_rd_n_o,
  output wire bus_wr_n_o,
  output wire bus_rfsh_n_o,
  input  wire bus_busreq_n_i,
  output wire bus_busack_n_o,
  input  wire bus_iorqula_n_i,

  // VGA
  output wire [2:0] rgb_r_o,
  output wire [2:0] rgb_g_o,
  output wire [2:0] rgb_b_o,
  output wire hsync_o,
  output wire vsync_o,
  output wire csync_o,

  // HDMI
  output wire [3:0] hdmi_p_o,
  output wire [3:0] hdmi_n_o,

  // I2C (RTC and HDMI)
  inout  wire i2c_scl_io,
  inout  wire i2c_sda_io,

  // ESP
  inout  wire esp_gpio0_io,
  inout  wire esp_gpio2_io,
  input  wire esp_rx_i,
  output wire esp_tx_o,

  // PI GPIO
  inout  wire [27:0] accel_io,

  // Vacant pins
  inout  wire extras_io

);

reg [7:0] mod;
reg mod_plus = 0;
reg mod_jmpst= 0;
reg mod_club = 0;
reg mod_orig = 0;
reg mod_bird = 0;
reg mod_ms   = 0;
reg mod_gork = 0;
reg mod_mrtnt= 0;
reg mod_woodp= 0;
reg mod_eeek = 0;
reg mod_alib = 0;
reg mod_ponp = 0;
reg mod_van  = 0;
reg mod_pmm  = 0;
reg mod_dshop= 0;
reg mod_glob = 0;

wire mod_gm = mod_gork | mod_mrtnt;

always @(posedge clk_sys) 
begin   
    mod_orig <= (mod == 0);
    mod_plus <= (mod == 1);
    mod_club <= (mod == 2);
    mod_bird <= (mod == 4);
    mod_ms   <= (mod == 5);
    mod_gork <= (mod == 6);
    mod_mrtnt<= (mod == 7);
    mod_woodp<= (mod == 8);
    mod_eeek <= (mod == 9);
    mod_alib <= (mod == 10);
    mod_ponp <= (mod == 11);
    mod_van  <= (mod == 12);
    mod_pmm  <= (mod == 13);
    mod_dshop<= (mod == 14);
    mod_glob <= (mod == 15);
    mod_jmpst<= (mod == 16);
end

reg scandblctrl = 1'b1;
reg [1:0] rotatectrl  = 2'b00;

reg joysel_s = 0;
reg [5:0] joy1_pins_s;
reg [5:0] joy2_pins_s;

always @(posedge clk_sys)
begin
        joysel_s <= ~joysel_s;
        
        if (joysel_s == 0)
            joy1_pins_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
        else
           joy2_pins_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
end 

assign joysel_o = joysel_s;
    
//assign LED = 1;
assign audioext_r_o = audioext_l_o;

wire clk_sys, clk_snd, clk_pixel, clk_dvi, clk_dvi_n;
reg pll_locked = 1'b0;

/*pll pll
(
    .CLK_IN1    (clock_50_i),
    .CLK_OUT1 (clk_sys),
    .CLK_OUT2 (clk_pixel),
    .CLK_OUT3 (clk_dvi),
    .CLK_OUT4 (clk_dvi_n),
    .LOCKED (pll_locked)
);
*/
reg [4:0] poweron_counter   = 5'b11111;
reg reset_poweron;
reg video_timing_change = 1'b1;
reg actual_video_mode = 1'b1;
    
pll_top pll 
(
    .SSTEP       ( reset_poweron ),      
    .STATE       ( rotatectrl[0] ),     
    .RST         ( 1'b0 ),
    .CLKIN       ( clock_50_i ),
    .SRDY        ( ),
 
    .CLK0OUT     ( clk_sys   ),          //-- Core 24 MHz
    .CLK1OUT     ( clk_pixel ),          //-- Pixel clock
    .CLK2OUT     ( clk_dvi   ),          //-- HDMI (pixel clock * 5)
    .CLK3OUT     ( clk_dvi_n ),          //-- HDMI inverted (180o)
    .CLK4OUT     ( ),           
    .CLK5OUT     ( )          
);

always @(posedge clk_sys) 
begin
        if (video_timing_change) 
            begin
                actual_video_mode <= rotatectrl[0];
                poweron_counter <= 5'b11111;
                reset_poweron <= 1'b1;
            end
        else
            if (poweron_counter > 5'b00000) 
                poweron_counter <= poweron_counter - 1;
            else
                reset_poweron <= 1'b0;
    
    video_timing_change <= (rotatectrl[0] != actual_video_mode) ? 1'b1 : 1'b0;
end 
    
reg ce_6m;
always @(posedge clk_sys) begin : b0
    reg [1:0] div;
    div <= div + 1'd1;
    ce_6m <= !div;
end

reg ce_4m;
always @(posedge clk_sys) begin : b1
    reg [2:0] div;
    
    div <= div + 1'd1;
    if(div == 5) div <= 0;
    ce_4m <= !div;
end

reg ce_1m79;
always @(posedge clk_sys) begin : b2
    reg [3:0] div;
    
    div <= div + 1'd1;
    if(div == 12) div <= 0;
    ce_1m79 <= !div;
end

wire [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire [10:0] ps2_key;
wire  [9:0] audio;
wire            hs, vs;
wire            hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] r,g;
wire  [1:0] b;
wire  [3:0] idx_col;

wire reset = ~btn_reset_n_i | dn_downloading;

wire [7:0] in0xor = mod_ponp ? 8'hE0 : 8'hFF;
wire [7:0] in1xor = mod_ponp ? 8'h00 : 8'hFF;
reg           m_cheat = 1'b0;

reg [7:0]rom_lo;
reg [7:0]rom_hi;
reg [15:0]rom_addr_s;

reg [7:0]dipsw1;
reg [7:0]dipsw2 = 8'b11111111;

always @(*) //status 23:16
begin   

    //defaults
    dipsw1[7]   <= 1'b1;  //-- character set 
    dipsw1[6]   <= 1'b1;  //-- difficulty ?
    dipsw1[5:4] <= 2'b00; //-- bonus pacman at 10K
    dipsw1[3:2] <= 2'b10; //-- lives (3)
    dipsw1[1:0] <= 2'b01; //-- cost  (1 coin, 1 play)   

        

    if ( mod_club )
    begin
        dipsw1[2] <= 1'b0; //-- lives (3)
    end 
    
    if ( mod_mrtnt | mod_gork )
    begin
        dipsw1[3:2] <= 2'b01; //-- lives (3)
        dipsw1[1:0] <= 2'b11; //-- cost  (1 coin, 1 play)   
    end 
    
    if ( mod_eeek | mod_glob )
    begin
        dipsw1[4:2] <= 3'b100; //-- difficulty (4)
        dipsw1[1:0] <= 2'b11; //-- lives (3)
    end 

    if ( mod_van | mod_dshop)
    begin
       dipsw1[7:6] <= 2'b11; //-- cost  (1 coin, 1 play)    
        dipsw1[5:4] <= 2'b11; //-- lives (3)
        dipsw1[3:2] <= 2'b00; //-- bonus
        dipsw1[1] <= 1'b0; //-- flip
        dipsw1[0] <= 1'b0; //-- upright
    end 

    if ( mod_jmpst )
    begin
        dipsw1[5] <= 1'b1; //-- cost  (1 coin, 1 play)
        dipsw1[4] <= 1'b1; //-- free play   
        dipsw1[3:2] <= 2'b11; //-- color
        dipsw1[1:0] <= 2'b11; //-- time
    end 

end 
    
pacmant pacmant
(
    .O_VIDEO_COL    ( idx_col ),
    .O_HSYNC        ( hs ),
    .O_VSYNC        ( vs ),
    .O_HBLANK       ( hb ),
    .O_VBLANK       ( vb ),
    .O_AUDIO        ( audio ),

    .in0( (in0xor ^ 
                            {
                                mod_eeek & m_fire2A,
                                mod_alib & btn_fireA,
                                btn_coin, 
                                (( mod_orig | mod_plus | mod_ms | mod_bird | mod_alib | mod_woodp) & m_cheat ) | (( mod_ponp | mod_van | mod_dshop ) & btn_fireA ),
                                btn_down,
                                btn_right,
                                btn_left,
                                btn_up
                            })),

    .in1({8'b11111111} & (in1xor ^   // [7] - cocktail/upright in most games
                            {
                                mod_gm & m_fire2A,
                                btn_two_players         | ( mod_eeek  & btn_fireA ) | ( mod_jmpst & m_fire2A ),
                                btn_one_player          | ( mod_jmpst & btn_fireA ),
                                ( mod_gm & btn_fireA ) | (( mod_alib | mod_ponp | mod_van | mod_dshop ) & m_fire2A ),
                                m_down2,
                                 mod_pmm ? btn_fireA : m_right2,
                                ~mod_pmm & m_left2,
                                 m_up2
                            })),
                        
    .dipsw1             ( dipsw1 ),
    .dipsw2             ( (mod_ponp | mod_van | mod_dshop) ? dipsw2 : 8'hFF ),


    .RESET              ( reset   ),
    .CLK                ( clk_sys ),
    .ENA_6              ( ce_6m   ),
    .ENA_4              ( ce_4m   ),
    .ENA_1M79           ( ce_1m79 ),

    // ---ROMS----

    .cpu_addr_o         ( cpu_addr_s ),
    .char_addr_o        ( char_addr_s ),
    .char_rom_5ef_dout  ( char_rom_5ef_dout ),

    .rom_addr           ( rom_addr_s ), 
    .rom_lo             ( rom_lo ),
    .rom_hi             ( rom_hi ),

    .video_x_o          ( video_x_s ),
    .video_y_o          ( video_y_s ),

    .mod_plus           ( mod_plus  ),
    .mod_jmpst          ( mod_jmpst ),
    .mod_bird           ( mod_bird  ),
    .mod_ms             ( mod_ms    ),
    .mod_mrtnt          ( mod_mrtnt ),
    .mod_woodp          ( mod_woodp ),
    .mod_eeek           ( mod_eeek ),
    .mod_alib           ( mod_alib ),
    .mod_ponp           ( mod_ponp | mod_van | mod_dshop ),
    .mod_van            ( mod_van | mod_dshop ),
    .mod_dshop          ( mod_dshop ),
    .mod_glob           ( mod_glob ),
    .mod_club           ( mod_club ),

    .dn_addr            ( dn_addr[15:0] ),
    .dn_data            ( ram_data_io[7:0] ),
    .dn_wr              ( dn_wr )
);
    
dac #
(
    .C_bits(11)
)
dac
(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i({1'b0, audio}),
    .dac_o(audioext_l_o)
);

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = m_one_player  | membrane_s[7];
wire btn_two_players = m_two_players | membrane_s[8];
wire btn_coin        = m_coin1       | membrane_s[6];

wire btn_up    = m_up    | membrane_s[0];
wire btn_down  = m_down  | membrane_s[1];
wire btn_left  = m_left  | membrane_s[2];
wire btn_right = m_right | membrane_s[3];
wire btn_fireA = m_fireA | membrane_s[4] | membrane_s[5];

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;
wire [9:0] membrane_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

membrane_joystick membrane_joystick 
(

  .clock            ( clk_sys ),
  .reset            ( reset ),

  .membrane_joy_o   ( membrane_s ), //-- P2, P1, COIN, Fire 2, Fire 1, R, L, D, U
    
  .keyb_row_o       ( keyb_row_o ),   
  .i_membrane_cols  ( keyb_col_i )

);
    
wire [15:0] joy1_s;
wire [15:0] joy2_s;
wire [8:0]  controls_s;
wire [12:1] fkeys_s;

//translate scancode to joystick
kbd_joystick k_joystick
(
    .clk            ( clk_sys ),
    .kbdint         ( kbd_intr ),
    .kbdscancode    ( kbd_scancode ), 

    .joystick_0     ({ joy1_pins_s[5:4], joy1_pins_s[0], joy1_pins_s[1], joy1_pins_s[2], joy1_pins_s[3] }),
    .joystick_1     ({ joy2_pins_s[5:4], joy2_pins_s[0], joy2_pins_s[1], joy2_pins_s[2], joy2_pins_s[3] }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( ~(mod_club | mod_pmm | mod_jmpst) ),

    //-- tilt, coin4-1, start4-1
    .controls       ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1        ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2        ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o          (  ),
    .osd_enable     ( 1'b0 ),

    //-- sega joystick
    .sega_clk       ( hs ),
    .sega_strobe    ( joyp7_o ),

    //-- Function keys
    .F_keys         ( fkeys_s )       
);

//--------- Scandoubler and scanlines_mode toggles ----------------------------------------------------
wire btn_scandb_s;
debounce #(.counter_size  (3)) debounce_nmi1
(
   .clk_i     ( clk_sys ),
   .button_i  ( ~btn_multiface_n_i & membrane_s[8]),
   .result_o  ( btn_scandb_s )   
  );
  
wire btn_scanln_s;
debounce # (.counter_size  (3)) debounce_nmi2
(
   .clk_i     ( clk_sys ),
   .button_i  ( ~btn_multiface_n_i & membrane_s[0]),
   .result_o  ( btn_scanln_s )   
  );
  
reg [1:0]scandb_edge;
reg [1:0]scanln_edge;
reg [1:0]rotate_edge;

always @(posedge clk_sys)
begin
      if (reset) 
      begin
          scandblctrl = 1'b1;
          scanlines_mode = 2'b00;
      end
      else
      begin
      
          scandb_edge = {scandb_edge[0],btn_scandb_s | fkeys_s[2]};
          if (scandb_edge == 2'b01) scandblctrl = ~scandblctrl;

          rotate_edge = {rotate_edge[0],m_tilt | membrane_s[9]};
          if (rotate_edge == 2'b01) rotatectrl = rotatectrl + 1;
          
          scanln_edge = {scanln_edge[0],btn_scanln_s | fkeys_s[7]};
          if (scanln_edge == 2'b01) scanlines_mode = scanlines_mode + 1;

      end 
end

//--------- RGB OUTPUT ----------------------------------------------------

wire [7:0] rgb_out;

dpram #(4,8) col_rom_rgb
(
    .clk_a_i    ( clk_pixel ),
    .en_a_i     ( 1'b1 ),
    .we_i       ( dn_wr & rom7_cs & prom_cs ),
    .addr_a_i   ( dn_addr[3:0] ), 
    .data_a_i   ( ram_data_io[7:0] ),

    .clk_b_i    ( clk_sys ),
    .en_b_i     ( 1'b1 ),
    .addr_b_i   ( idx_col ),
    .data_b_o   ( rgb_out )
);

assign rgb_r_o = ( scandblctrl ) ? vga_r_out_s[5:3] : rgb_out[2:0];
assign rgb_g_o = ( scandblctrl ) ? vga_g_out_s[5:3] : rgb_out[5:3];
assign rgb_b_o = ( scandblctrl ) ? vga_b_out_s[5:3] : {rgb_out[7:6], rgb_out[7]};
assign hsync_o = ( scandblctrl ) ? vga_hs_s         : ~hs;
assign vsync_o = ( scandblctrl ) ? vga_vs_s         : ~vs;

//--------- VGA/HDMI OUTPUT ----------------------------------------------------

wire [8:0] video_x_s;
wire [7:0] video_y_s;

wire [3:0] vga_col_s;
wire vga_blank_s;

//-- contador I_HCNT de 128 a 511 = 48 + 288 pixels horizontais
//-- hsync de 175 a 207
//-- hblank de 143 a 239
//-- imagem de 239 a 511 (272 pixels) e de 128 a 143 (16 pixels)

//-- contador I_VCNT de 248 a 511 = 39 + 224 pixels verticais   

vga vga
(
    .I_CLK      ( clk_sys ),
    .I_CLK_VGA  ( clk_pixel ),
    .I_COLOR    ( idx_col ),
    .I_HCNT     ( video_x_s ),
    .I_VCNT     ( video_y_s ),
    .I_ROTATE   ( rotatectrl ),
    
    .O_HSYNC    ( vga_hs_s ),
    .O_VSYNC    ( vga_vs_s ),
    .O_COLOR    ( vga_col_s ),
    .O_BLANK    ( vga_blank_s )
);

wire [7:0] video_out;

wire prom_cs;
wire rom7_cs;
assign prom_cs = (dn_addr[15:14] == 2'b11);    //range 0xc000-ffff 
assign rom7_cs = (dn_addr[9:4] == 6'b110000);  //0xc300-0xc30f

dpram #(4,8) col_rom_7f
(
    .clk_a_i    ( clk_pixel ),
    .en_a_i     ( 1'b1 ),
    .we_i       ( dn_wr & rom7_cs & prom_cs ),
    .addr_a_i   ( dn_addr[3:0] ), 
    .data_a_i   ( ram_data_io[7:0] ),

    .clk_b_i    ( clk_pixel ),
    .en_b_i     ( 1'b1 ),
    .addr_b_i   ( vga_col_s ),
    .data_b_o   ( video_out )
);

assign {b,g,r} = video_out;

wire [5:0] vga_r_s = { r, 3'b000 }; 
wire [5:0] vga_g_s = { g, 3'b000 }; 
wire [5:0] vga_b_s = { b, b[1], 4'b0000 };  
    
wire vga_hs_s;  
wire vga_vs_s;  

reg [5:0] vga_r_out_s;  
reg [5:0] vga_g_out_s;  
reg [5:0] vga_b_out_s;  

reg [1:0] scanlines_mode = 2'b00;
reg       scanline  = 1'b0;

reg [1:0]hs_edge;
reg [1:0]vs_edge;
    
always @(posedge clk_sys)
begin
    hs_edge = {hs_edge[0],vga_hs_s};
    vs_edge = {vs_edge[0],vga_vs_s};

    if (hs_edge == 2'b10) scanline <= ~scanline;
    if (vs_edge == 2'b10) scanline <= 1'b0;
end

//scanlines_mode 
always @(posedge clk_pixel) 
begin

    // if no scanlines_mode or not a scanline
    if(!scanline || !scanlines_mode) 
    begin
        vga_r_out_s <= vga_r_s;
        vga_g_out_s <= vga_g_s;
        vga_b_out_s <= vga_b_s;
    end else 
    begin
        case(scanlines_mode)
            2'b01: begin // reduce 25% = 1/2 + 1/4
                vga_r_out_s <= {1'b0, vga_r_s[5:1]} + {2'b00, vga_r_s[5:2] };
                vga_g_out_s <= {1'b0, vga_g_s[5:1]} + {2'b00, vga_g_s[5:2] };
                vga_b_out_s <= {1'b0, vga_b_s[5:1]} + {2'b00, vga_b_s[5:2] };
            end

            2'b10: begin // reduce 50% = 1/2
                vga_r_out_s <= {1'b0, vga_r_s[5:1]};
                vga_g_out_s <= {1'b0, vga_g_s[5:1]};
                vga_b_out_s <= {1'b0, vga_b_s[5:1]};
            end

            2'b11: begin // reduce 75% = 1/4
                vga_r_out_s <= {2'b00, vga_r_s[5:2]};
                vga_g_out_s <= {2'b00, vga_g_s[5:2]};
                vga_b_out_s <= {2'b00, vga_b_s[5:2]};
            end
        endcase
    end
end

wire [15:0] sound_hdmi_s;

wire [9:0] tdms_r_s;
wire [9:0] tdms_g_s;
wire [9:0] tdms_b_s;
wire [3:0] hdmi_p_s;
wire [3:0] hdmi_n_s;

//-- HDMI
hdmi 
# (
    .FREQ ( 2700000 ),   //-- pixel clock frequency 
    .FS   ( 48000 ),      //-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
    .CTS  ( 27000 ),      //-- CTS = Freq(pixclk) * N / (128 * Fs)
    .N    ( 6144 )        //-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
) inst_dvid
(
    .I_CLK_PIXEL    ( clk_pixel ),
    .I_R            ( {vga_r_out_s, vga_r_out_s[5:4]} ),
    .I_G            ( {vga_g_out_s, vga_g_out_s[5:4]} ),
    .I_B            ( {vga_b_out_s, vga_b_out_s[5:4]} ),
    .I_BLANK        ( vga_blank_s ),
    .I_HSYNC        ( vga_hs_s ),
    .I_VSYNC        ( vga_vs_s ),
    
    //-- PCM audio
    .I_AUDIO_ENABLE ( 1'b1 ),
    .I_AUDIO_PCM_L  ( sound_hdmi_s ),
    .I_AUDIO_PCM_R  ( sound_hdmi_s ),
    
    //-- TMDS parallel pixel synchronous outputs (serialize LSB first)
    .O_RED          ( tdms_r_s ),
    .O_GREEN        ( tdms_g_s ),
    .O_BLUE         ( tdms_b_s )
);
  
  hdmi_out_xilinx hdmio
  (
      .clock_pixel_i  ( clk_pixel ),
      .clock_tdms_i   ( clk_dvi   ),
      .clock_tdms_n_i ( clk_dvi_n ),
      .red_i          ( tdms_r_s ),
      .green_i        ( tdms_g_s ),
      .blue_i         ( tdms_b_s ),
      .tmds_out_p     ( hdmi_p_o ),
      .tmds_out_n     ( hdmi_n_o )
  );
           
  wire [15:0] pcm_s;
  audio_pcm
  #( .CLK_RATE (40000000))
  audio_pcm
  (
      .clk         ( clk_pixel ),
      .sample_rate ( 0 ), //48khz
      .left_in     ( {audio, 6'b000000} ),
      .pcm_l       ( pcm_s )
  );

  assign sound_hdmi_s =  {1'b0, ~pcm_s[15], pcm_s[14:1]}; 

  //-------------------
  // ROMS
  
  wire [15:0] cpu_addr_s;
//  wire [7:0]  program_rom_dinl;
  
  wire [12:0] char_addr_s;
  reg  [7:0]  char_rom_5ef_dout;
  wire [7:0]  gfx_dout;
  reg  char_oe;
  
  assign ram_oe_n_o   = 1'b0; 
  assign ram_ce_n_o = 4'b1110;
//  assign program_rom_dinl = ram_data_io[7:0]; 
  
//  assign sram_addr_o  = (ioctl_wr) ? ioctl_addr[18:0] : {5'b00000,cpu_addr_s[13:0] }; 
//  assign sram_data_io = (ioctl_wr) ? ioctl_dout : 8'hZZ;
//  assign program_rom_dinl = sram_data_io;
//  assign sram_we_n_o  = ~ioctl_wr;

  always @(*)
  begin
      if (char_oe) char_rom_5ef_dout <= ram_data_io[7:0];

      if (rom_addr_s[15]) rom_hi <= ram_data_io[7:0]; else rom_lo <= ram_data_io[7:0];    
      
      if (ram_addr_o[15:0] == 16'hc310 && dn_wr == 1'b1)  mod <= ram_data_io[7:0];    
  end 
  
  always @(posedge clk_sys)
  begin                                                            //0x8000 (graphics)         0x4000-7fff (HI rom)   0x0000-3fff (LO rom)
      ram_addr_o  <= (dn_downloading) ? dn_addr[18:0] : (ce_6m) ? {6'b110100,char_addr_s } : {4'b1100,rom_addr_s[15], rom_addr_s[13:0] }; //second half of the SRAM
      //ram_data_io <= 8'hZZ;
      ram_we_n_o   <= 1'b1;
      char_oe <= ce_6m; //delay the clock signal one cycle to make the OE for video data
  end

  reg [ 19:0] counter_addr;
  wire [18:0] dn_addr = counter_addr[19:1];
  reg         dn_wr;
  reg         dn_downloading;

  //--------- PROM DATA PUMP --------------------------------
  wire hard_reset = ~pll_locked;

  //--start after the FPGA power on
  always @(posedge ce_6m) 
  begin

          if (hard_reset == 1)
              begin
                 pll_locked <= 1'b1;
                  dn_downloading = 1'b1;
                  counter_addr = 20'hd8000; //0xc000 (shifted)
                  dn_wr = 1'b1;
              end
          else if (counter_addr < 20'hd8622) //0xc311 (shifted)
              begin
                  counter_addr = counter_addr + 1;
                  dn_wr = counter_addr[0]; //use the LSB bit as write clock
              end 
          else
              begin
                  dn_downloading = 1'b0;
                  dn_wr = 1'b0;
              end 
  end 
  //---------------------------------------------------------
  
  //--------------------------------------------------------
  //-- Reboot to Spectrum Next Core
  //--------------------------------------------------------
  
  reg [4:0] zxn_coreid = 5'b00001; // Spectrum Next slot 
  flashboot flashboot
  (
      .reset_i        ( hard_reset ),
      .clock_i        ( clk_sys ),
      .start_i        ( ~(btn_divmmc_n_i | btn_reset_n_i) ),
      .spiaddr_i  ( {8'h6B, zxn_coreid, 19'b0000000000000000000} )
  );

  //--------------------------------------------------------
  //-- Unused outputs
  //--------------------------------------------------------

  // -- Interal audio (speaker, not fitted)
  assign audioint_o     = 1'b0;

  //-- Spectrum Next Bus
  assign bus_addr_o     = 16'hFFF0;
  assign bus_busack_n_o = 1'b1;
  assign bus_clk35_o    = 1'b1;
  assign bus_data_io    = 8'hFF;
  assign bus_halt_n_o   = 1'b1;
  assign bus_iorq_n_o   = 1'b1;
  assign bus_m1_n_o     = 1'b1;
  assign bus_mreq_n_o   = 1'b1;
  assign bus_rd_n_o     = 1'b1;
  assign bus_rfsh_n_o   = 1'b1;
  assign bus_rst_n_io   = 1'b1;
  assign bus_wr_n_o     = 1'b1;

  //-- ESP 8266 module
  assign esp_gpio0_io   = 1'bZ;
  assign esp_gpio2_io   = 1'bZ;
  assign esp_tx_o = 1'b1;

  //-- Addtional flash pins; used at IO2 and IO3 in Quad SPI Mode
  assign flash_hold_o   = 1'b1;
  assign flash_wp_o     = 1'b1;

  assign flash_cs_n_o  = 1'b1;
  assign flash_sclk_o  = 1'b1;
  assign flash_mosi_o  = 1'b1;

  assign ear_port_i = 1'b1;

  assign i2c_scl_io = 1'bZ;
  assign i2c_sda_io = 1'bZ;

  //-- Mic Port (output, as it connects to the mic input on cassette deck)
  assign mic_port_o = 1'b0;

  //-- CS2 is for internal SD socket
  assign sd_cs1_n_o = 1'b1;

  // PI GPIO
  assign accel_io = 28'bZZZZZZZZZZZZZZZZZZZZZZZZZZZZ;

  // Vacant pins
  assign extras_io = 1'b1;

  assign csync_o = 1'b1;

  assign sd_cs0_n_o = 1'b1;
  assign sd_cs1_n_o = 1'b1;
  assign sd_sclk_o  = 1'b1;
  assign sd_mosi_o  = 1'b1;

endmodule 
