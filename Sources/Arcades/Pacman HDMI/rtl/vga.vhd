----------------------------------------------------------
--
-- Framebuffer for VGA output
-- 
-- Victor Trucco - 2020
--
----------------------------------------------------------

library IEEE; 
use IEEE.std_logic_1164.all; 
use IEEE.std_logic_unsigned.all;
use IEEE.numeric_std.all;
    
entity vga is
    port (
        I_CLK           : in  std_logic;
        I_CLK_VGA   : in  std_logic;
        I_COLOR     : in  std_logic_vector(3 downto 0);
        I_HCNT      : in  std_logic_vector(8 downto 0);
        I_VCNT      : in  std_logic_vector(7 downto 0);
        I_ROTATE    : in  std_logic_vector(1 downto 0);
        
        O_HSYNC     : out std_logic;
        O_VSYNC     : out std_logic;
        O_COLOR     : out std_logic_vector(3 downto 0);
        O_BLANK     : out std_logic
    );
end vga;

architecture rtl of vga is

    signal pixel_out        : std_logic_vector( 3 downto 0);
    signal addr_rd          : std_logic_vector(15 downto 0);
    signal addr_wr          : std_logic_vector(15 downto 0);
    signal wren             : std_logic;
    signal picture          : std_logic;
    signal window_hcnt  : std_logic_vector( 9 downto 0) := (others => '0');
    signal window_vcnt  : std_logic_vector( 9 downto 0) := (others => '0');
    signal hcnt             : std_logic_vector( 9 downto 0) := (others => '0');
    --signal h                  : std_logic_vector( 9 downto 0) := (others => '0');
    signal vcnt             : std_logic_vector( 9 downto 0) := (others => '0');
    signal hsync            : std_logic;
    signal vsync            : std_logic;
    signal blank            : std_logic;

    
    --ModeLine "720x480@60"       27.00    720   736   798   858      480   489   495   525
    --Modeline "720x576x50hz"       27      720   732   796   864      576   581   586   625 
    
    -- Horizontal Timing constants  
    signal h_pixels_across   : integer;
    signal h_sync_on         : integer;
    signal h_sync_off        : integer;
    signal h_end_count       : integer;
    -- Vertical Timing constants
    signal v_pixels_down     : integer;
    signal v_sync_on         : integer;
    signal v_sync_off        : integer;
    signal v_end_count       : integer;
    
    -- In   
    constant hc_max         : integer := 288; -- Number of horizontal visible pixels (before scandoubler)
    constant vc_max         : integer := 224; -- Number of vertical visible pixels  (before scandoubler)

        -- VGA positioning
    signal h_start              : integer; -- initial X position on VGA Screen
    signal h_end                : integer;
    signal v_start              : integer; -- initial Y position on VGA screen
    signal v_end                : integer;
    
begin

    process (I_CLK_VGA)
    begin
        if I_CLK_VGA'event and I_CLK_VGA = '1' then 
            if I_ROTATE(0) = '0' then
            
                h_pixels_across <= 720 - 1;
                h_sync_on       <= 736 - 1;
                h_sync_off      <= 798 - 1;
                h_end_count     <= 858 - 1;
            
                v_pixels_down   <= 480 - 1;
                v_sync_on       <= 489 - 1;
                v_sync_off      <= 495 - 1;
                v_end_count     <= 525 - 1;
                
                h_start         <= 32; -- initial X position on VGA Screen
                h_end           <= h_start + (hc_max * 2);
                v_start         <= 16; -- initial Y position on VGA screen
                v_end           <= v_start + (vc_max * 2);

            else

            --  720x576
      --          h_pixels_across     <= 720 - 1;
      --          h_sync_on           <= 732 - 1;
      --          h_sync_off          <= 796 - 1;
      --          h_end_count         <= 864 - 1;
      --      
      --          v_pixels_down       <= 576 - 1;
      --          v_sync_on           <= 581 - 1;
      --          v_sync_off          <= 586 - 1;
      --          v_end_count         <= 625 - 1;
                
            --  800x600
                h_pixels_across     <= 800 - 1;
                h_sync_on           <= 840 - 1;
                h_sync_off          <= 968 - 1;
                h_end_count         <= 1056 - 1;
            
                v_pixels_down       <= 600 - 1;
                v_sync_on           <= 601 - 1;
                v_sync_off          <= 605 - 1;
                v_end_count         <= 628 - 1;
                      
                h_start             <= 136; -- initial X position on VGA Screen
                h_end               <= h_start + (vc_max * 2);
                v_start             <= 0; -- initial Y position on VGA screen
                v_end               <= v_start + (hc_max * 2);

            end if;
        end if;
    end process;
    
    framebuffer: entity work.dpram2
    generic map 
    (
        addr_width_g    => 16,
        data_width_g    => 4
    )
    port map
    (
        clk_a_i     => I_CLK,
        data_a_i    => I_COLOR,
        addr_a_i    => addr_wr,
        we_i        => wren,
        data_a_o    => open,
        --
        clk_b_i     => I_CLK_VGA,
        addr_b_i    => addr_rd,
        data_b_o    => pixel_out
    );

    process (I_CLK_VGA)
    begin
        if I_CLK_VGA'event and I_CLK_VGA = '1' then 
            if hcnt = h_end_count then
                hcnt <= (others => '0');
            else
                hcnt <= hcnt + 1;
                if hcnt = h_start then
                    window_hcnt <= (others => '0');
                else
                    window_hcnt <= window_hcnt + 1;
                end if;
            end if;
            
            if hcnt = h_sync_on then
                if vcnt = v_end_count then
                    vcnt <= (others => '0');
                else
                    vcnt <= vcnt + 1;
                    if vcnt = v_start then
                        window_vcnt <= (others => '0');
                    else
                        window_vcnt <= window_vcnt + 1;
                    end if;
                end if;
            end if;
        end if;
    end process;

    process (I_HCNT, I_VCNT, window_hcnt, window_vcnt)
        variable wr_result_v : std_logic_vector(16 downto 0);
        variable rd_result_v : std_logic_vector(17 downto 0);
    begin
        wr_result_v := std_logic_vector((unsigned(I_VCNT)                  * to_unsigned(hc_max, 9)) + unsigned(I_HCNT));
        
        if (I_ROTATE = "00") then -- no rotation
            rd_result_v := '0' & std_logic_vector((unsigned(window_vcnt(8 downto 1)) * to_unsigned(hc_max, 9)) + unsigned(window_hcnt(9 downto 1)));


        elsif (I_ROTATE = "01") then  -- 90o CW           
            rd_result_v := std_logic_vector
            (                                
                                        (((to_unsigned(vc_max, 9) -1) - (unsigned(window_hcnt(9 downto 1)))) * to_unsigned(hc_max, 9)) + 
                                        (unsigned(window_vcnt(9 downto 1)))                            
            ); 
        elsif (I_ROTATE = "10") then -- 180o  
            rd_result_v := std_logic_vector
            (                                
                                       (((to_unsigned(vc_max, 9) - (unsigned(window_vcnt(9 downto 1)))) * to_unsigned(hc_max, 9)) - 
                                        (unsigned(window_hcnt(9 downto 1)))-1)                        
            ); 

        else -- 90o CCW  (( h *16 + (16-v)-1) ));
            rd_result_v := std_logic_vector
            (                                
                                        (to_unsigned(hc_max, 9) * (unsigned(window_hcnt(9 downto 1)))) + 

                                        (to_unsigned(hc_max, 9) - (unsigned(window_vcnt(9 downto 1)))) - 1                         
            ); 

        end if;
        
        addr_wr <= wr_result_v(15 downto 0);
        addr_rd <= rd_result_v(15 downto 0);
    end process;

    wren        <= '1' when (I_HCNT < hc_max) and (I_VCNT < vc_max) else '0';
--  addr_wr <= I_VCNT(7 downto 0) & I_HCNT(7 downto 0);
--  addr_rd <= window_vcnt(8 downto 1) & window_hcnt(8 downto 1);
    blank       <= '1' when (hcnt > h_pixels_across) or (vcnt > v_pixels_down) else '0';
    
    picture <= '1' when (hcnt > h_start+1 and hcnt <= h_end) and (vcnt > v_start and vcnt <= v_end) else '0';

    O_HSYNC <= '1' when (hcnt <= h_sync_on) or (hcnt > h_sync_off) else '0';
    O_VSYNC <= '1' when (vcnt <= v_sync_on) or (vcnt > v_sync_off) else '0';
--  O_COLOR <= pixel_out when picture = '1' and (blank = '0') else (others => '0');
    
    O_COLOR <= --"1111"      when hcnt = 0 or hcnt = h_pixels_across or vcnt=0 or vcnt=v_pixels_down else 
                    --"1111"     when hcnt = h_start or hcnt = h_end else 
                    pixel_out when picture = '1' and (blank = '0') else 
                    (others => '0');
    
    O_BLANK <= blank;

end rtl;
