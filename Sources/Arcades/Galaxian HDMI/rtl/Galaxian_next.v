//============================================================================
//  Arcade: Galaxian
//
//  Top with full framebuffer and HDMI
//  Copyright (C) 2020 Victor Trucco
//
//  This program is free software; you can redistribute it and/or modify it
//  under the terms of the GNU General Public License as published by the Free
//  Software Foundation; either version 2 of the License, or (at your option)
//  any later version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
//  more details.
//
//  You should have received a copy of the GNU General Public License along
//  with this program; if not, write to the Free Software Foundation, Inc.,
//  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//============================================================================

//============================================================================
//
//  ZX Spectrum Next Top by Victor Trucco 2020
//
//============================================================================

`default_nettype none

module Galaxian_next
(
    // Clocks
  input  wire clock_50_i,

  //SRAM (AS7C34096)
  output reg [18:0] ram_addr_o,
  inout  wire [15:0] ram_data_io,
  output wire ram_oe_n_o,
  output wire ram_we_n_o,
  output wire [3:0] ram_ce_n_o,

 // PS2
  inout wire ps2_clk_io,
  inout wire ps2_data_io,
  inout wire ps2_pin6_io,
  inout wire ps2_pin2_io,
                        
  // SD Card
  output wire sd_cs0_n_o,
  output wire sd_cs1_n_o,
  output wire sd_sclk_o,
  output wire sd_mosi_o,
  input  wire sd_miso_i,

  // Flash
  output wire flash_cs_n_o,
  output wire flash_sclk_o,
  output wire flash_mosi_o,
  input  wire flash_miso_i,
  output wire flash_wp_o,
  output wire flash_hold_o,

  // Joystick
  input  wire joyp1_i,
  input  wire joyp2_i,
  input  wire joyp3_i,
  input  wire joyp4_i,
  input  wire joyp6_i,
  output wire joyp7_o,
  input  wire joyp9_i,
  output wire joysel_o,

  // Audio
  output wire audioext_l_o,
  output wire audioext_r_o,
  output wire audioint_o,

  // K7
  input  wire ear_port_i,
  output wire mic_port_o,

  // Buttons
  input  wire btn_divmmc_n_i,
  input  wire btn_multiface_n_i,
  input  wire btn_reset_n_i,

  // Matrix keyboard
  output wire [7:0] keyb_row_o,
  input  wire [6:0] keyb_col_i,

  // Bus
  inout  wire bus_rst_n_io,
  output wire bus_clk35_o,
  output wire [15:0] bus_addr_o,
  inout  wire [7:0] bus_data_io,
  inout  wire bus_int_n_io,
  input  wire bus_nmi_n_i,
  input  wire bus_ramcs_i,
  input  wire bus_romcs_i,
  input  wire bus_wait_n_i,
  output wire bus_halt_n_o,
  output wire bus_iorq_n_o,
  output wire bus_m1_n_o,
  output wire bus_mreq_n_o,
  output wire bus_rd_n_o,
  output wire bus_wr_n_o,
  output wire bus_rfsh_n_o,
  input  wire bus_busreq_n_i,
  output wire bus_busack_n_o,
  input  wire bus_iorqula_n_i,

  // VGA
  output wire [2:0] rgb_r_o,
  output wire [2:0] rgb_g_o,
  output wire [2:0] rgb_b_o,
  output wire hsync_o,
  output wire vsync_o,
  output wire csync_o,

  // HDMI
  output wire [3:0] hdmi_p_o,
  output wire [3:0] hdmi_n_o,

  // I2C (RTC and HDMI)
  inout  wire i2c_scl_io,
  inout  wire i2c_sda_io,

  // ESP
  inout  wire esp_gpio0_io,
  inout  wire esp_gpio2_io,
  input  wire esp_rx_i,
  output wire esp_tx_o,

  // PI GPIO
  inout  wire [27:0] accel_io,

  // Vacant pins
  inout  wire extras_io

);


`define NAME "GALAXIAN"
reg [6:0] core_mod;
reg rotate_dir;

always @(*) 
begin

 //   core_mod = 7'd0;
    
    if (core_mod == 7'd1  || // "MOONCR"
        core_mod == 7'd6  || // "DEVILFSH"
        core_mod == 7'd9  || // "OMEGA"
        core_mod == 7'd10 || // "ORBITRON"
        core_mod == 7'd13)   // "VICTORY"
    begin
        rotate_dir = 1'b0;
    end else begin
        rotate_dir = 1'b1;
    end
end


reg scandblctrl = 1'b1;
reg [1:0] rotatectrl  = 2'b00;

reg joysel_s = 0;
reg [5:0] joy1_pins_s;
reg [5:0] joy2_pins_s;

always @(posedge clk_24)
begin
        joysel_s <= ~joysel_s;
        
        if (joysel_s == 0)
            joy1_pins_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
        else
           joy2_pins_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
end 

assign joysel_o = joysel_s;

assign audioext_r_o = audioext_l_o;


wire clk_24, clk_snd, clk_pixel, clk_dvi, clk_dvi_n;
wire clk_6, clk_12;
reg pll_locked = 1'b0;

reg [4:0] poweron_counter   = 5'b11111;
reg reset_poweron;
reg video_timing_change = 1'b1;
reg actual_video_mode = 1'b1;
    
pll_top pll 
(
    .SSTEP       ( reset_poweron ),      
    .STATE       ( rotatectrl[0] ),     
    .RST         ( 1'b0 ),
    .CLKIN       ( clock_50_i ),
    .SRDY        ( ),
 
    .CLK0OUT     ( clk_24   ),           //-- Core 24 MHz
    .CLK1OUT     ( clk_12 ),          
    .CLK2OUT     ( clk_6   ),          
    .CLK3OUT     ( clk_pixel ),          //-- Pixel clock
    .CLK4OUT     ( clk_dvi   ),          //-- HDMI (pixel clock * 5)
    .CLK5OUT     ( clk_dvi_n )           //-- HDMI inverted (180o)      
);

always @(posedge clk_24) 
begin
        if (video_timing_change) 
            begin
                actual_video_mode <= rotatectrl[0];
                poweron_counter <= 5'b11111;
                reset_poweron <= 1'b1;
            end
        else
            if (poweron_counter > 5'b00000) 
                poweron_counter <= poweron_counter - 1;
            else
                reset_poweron <= 1'b0;
    
    video_timing_change <= (rotatectrl[0] != actual_video_mode) ? 1'b1 : 1'b0;
end 





reg  [31:0] status;
wire  [1:0] buttons;
wire  [1:0] switches;
wire  [7:0] joystick_0;
wire  [7:0] joystick_1;
wire        scandoublerD;
wire        ypbpr;
wire        no_csync;
wire        key_strobe;
wire        key_pressed;
wire  [7:0] key_code;


/* ROM Structure
0000 - 3FFF PGM ROM 16k
4000 - 4FFF ROM 1K   4k
5000 - 5FFF ROM 1H   4k
6000 - 601F ROM 6L  32b
*/

wire  [7:0] audio_a, audio_b, audio_c;
wire [10:0] audio = {1'b0, audio_b, 2'b0} + {3'b0, audio_a} + {2'b00, audio_c, 1'b0};
wire        hs, vs;
wire        hb, vb;
wire        blankn = ~(hb | vb);
wire  [2:0] r,g,b;
wire  [4:0] idx_color_s;

wire reset = ~btn_reset_n_i | dn_downloading;

wire W_MISSILE, W_SHELL;
wire [4:0] W_STARS;

wire [11:0] gfx_addr_s;
wire [12:0] explosion_addr_s;
wire [13:0] rom_addr_s;
reg  [ 7:0] rom_dout_s, gfx_data_s,explosion_data_s;
wire rom_cs_s;

always @(*) //status 23:16
begin   



    status[5]     <= 1'b0; // table 0 = upright 1 = cocktail 
    status[6]     <= 1'b0; // test
    status[9]     <= 1'b0; // service mode
    status[15:14] <= 2'b00;
    status[23:16] <= 8'b00000000;

    if ( core_mod == 7'd0 || core_mod == 7'd4 || core_mod == 7'd7 || core_mod == 7'd9 || core_mod == 7'd13 ) // Galaxian, Catacomb, King Ballons, Omega, Victory
    begin
        status[18] <= 1'b1; //-- lives (3)
    end 
    
    if ( core_mod == 7'd1 || core_mod == 7'd4 || core_mod == 7'd7  || core_mod == 7'd9 ) // Moon Cresta, Catacomb, King Ballons, Omega
    begin
        status[14] <= 1'b1; //-- 1c/1cr
    end 
    
    if ( core_mod == 7'd14 ) // War of the bugs
    begin
        status[15:14] <= 2'b10; //-- lives (3)
    end 

end 


galaxian galaxian
(
    .W_CLK_24M      ( clk_24   ),
    .W_CLK_12M      ( clk_12   ),
    .W_CLK_6M       ( clk_6    ),
    .I_RESET        ( reset    ),
    .I_HWSEL        ( core_mod ),

    .I_DL_ADDR      ( dn_addr[15:0] ),
    .I_DL_WR        ( dn_wr         ),
    .I_DL_DATA      ( dn_data       ), 

    .I_TABLE        ( status[5] ),
    .I_TEST         ( status[6] ),
    .I_SERVICE      ( status[9] ),
    .I_SW1_67       ( status[15:14] ),
    .I_DIP          ( status[23:16] ),
    .P1_CSJUDLR     ( {btn_coin,btn_one_player,btn_fireA,btn_up,btn_down,btn_left,btn_right} ),
    .P2_CSJUDLR     ( {1'b0,btn_two_players,m_fire2A,m_up2,m_down2,m_left2,m_right2} ),

    .video_x_o      ( video_x_s ),
    .video_y_o      ( video_y_s ),

    .W_MISSILE      ( W_MISSILE ),
    .W_SHELL        ( W_SHELL   ),
    .W_STARS        ( W_STARS   ),

    .CLUT_ADDR_O    ( idx_color_s ),
    .W_R            ( r ),
    .W_G            ( g ),
    .W_B            ( b ),
    .W_H_SYNC       ( hs ),
    .W_V_SYNC       ( vs ),
    .HBLANK         ( hb ),
    .VBLANK         ( vb ),

    .W_SDAT_A       ( audio_a ),
    .W_SDAT_B       ( audio_b ),
    .W_SDAT_C       ( audio_c ),

    // external roms
    .W_CPU_ROM_ADDR   ( rom_addr_s ),
    .W_CPU_ROM_DO     ( rom_dout_s ),
    .ROM_CS_O         ( rom_cs_s   ),
    .GFX_ADDR_O       ( gfx_addr_s ),
    .GFX_DATA_I       ( gfx_data_s ),
    .explosion_addr_o ( explosion_addr_s ),
    .explosion_data_i ( explosion_data_s )

);



wire [2:0] comb_r = r | color_s[2:0]; 
wire [2:0] comb_g = g | color_s[5:3]; 
wire [2:0] comb_b = b | {color_s[7:6], 1'b0}; 

wire [5:0] vga_r_s; 
wire [5:0] vga_g_s; 
wire [5:0] vga_b_s; 
wire vga_hs_s, vga_vs_s;

reg [5:0] vga_r_out_s; 
reg [5:0] vga_g_out_s; 
reg [5:0] vga_b_out_s; 
    
reg [1:0] scanlines_mode = 2'b00;
reg       scanline  = 1'b0;
    
    //scanlines
    always @(posedge clk_pixel) 
    begin

        // if no scanlines_ctrl or not a scanline
        if(!scanline || !scanlines_mode) 
        begin
            vga_r_out_s <= vga_r_s;
            vga_g_out_s <= vga_g_s;
            vga_b_out_s <= vga_b_s;
        end else 
        begin
            case(scanlines_mode)
                2'b01: begin // reduce 25% = 1/2 + 1/4
                    vga_r_out_s <= {1'b0, vga_r_s[5:1]} + {2'b00, vga_r_s[5:2] };
                    vga_g_out_s <= {1'b0, vga_g_s[5:1]} + {2'b00, vga_g_s[5:2] };
                    vga_b_out_s <= {1'b0, vga_b_s[5:1]} + {2'b00, vga_b_s[5:2] };
                end

                2'b10: begin // reduce 50% = 1/2
                    vga_r_out_s <= {1'b0, vga_r_s[5:1]};
                    vga_g_out_s <= {1'b0, vga_g_s[5:1]};
                    vga_b_out_s <= {1'b0, vga_b_s[5:1]};
                end

                2'b11: begin // reduce 75% = 1/4
                    vga_r_out_s <= {2'b00, vga_r_s[5:2]};
                    vga_g_out_s <= {2'b00, vga_g_s[5:2]};
                    vga_b_out_s <= {2'b00, vga_b_s[5:2]};
                end
            endcase
        end
    end
            
assign rgb_r_o = ( scandblctrl ) ? vga_r_out_s[5:3] : comb_r;
assign rgb_g_o = ( scandblctrl ) ? vga_g_out_s[5:3] : comb_g;
assign rgb_b_o = ( scandblctrl ) ? vga_b_out_s[5:3] : comb_b;
assign hsync_o = ( scandblctrl ) ? vga_hs_s         : hs;
assign vsync_o = ( scandblctrl ) ? vga_vs_s         : vs;

dac #(11)
dac(
    .clk_i(clk_12),
    .res_n_i(1),
    .dac_i(audio),
    .dac_o(audioext_l_o)
    );

//-----------------------

wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = m_one_player  | membrane_s[7];
wire btn_two_players = m_two_players | membrane_s[8];
wire btn_coin        = m_coin1       | membrane_s[6];

wire btn_up    = m_up    | membrane_s[0];
wire btn_down  = m_down  | membrane_s[1];
wire btn_left  = m_left  | membrane_s[2];
wire btn_right = m_right | membrane_s[3];
wire btn_fireA = m_fireA | membrane_s[4] | membrane_s[5];

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;
wire [9:0] membrane_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_24 ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

membrane_joystick membrane_joystick 
(

  .clock            ( clk_24 ),
  .reset            ( reset ),

  .membrane_joy_o   ( membrane_s ), //-- P2, P1, COIN, Fire 2, Fire 1, R, L, D, U
    
  .keyb_row_o       ( keyb_row_o ),   
  .i_membrane_cols  ( keyb_col_i )

);
    
wire [15:0] joy1_s;
wire [15:0] joy2_s;
wire [8:0]  controls_s;
wire [12:1] fkeys_s;

//translate scancode to joystick
kbd_joystick k_joystick
(
    .clk            ( clk_24 ),
    .kbdint         ( kbd_intr ),
    .kbdscancode    ( kbd_scancode ), 

    .joystick_0     ({ joy1_pins_s[5:4], joy1_pins_s[0], joy1_pins_s[1], joy1_pins_s[2], joy1_pins_s[3] }),
    .joystick_1     ({ joy2_pins_s[5:4], joy2_pins_s[0], joy2_pins_s[1], joy2_pins_s[2], joy2_pins_s[3] }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls       ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1        ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2        ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o          (  ),
    .osd_enable     ( 1'b0 ),

    //-- sega joystick
    .sega_clk       ( hs ),
    .sega_strobe    ( joyp7_o ),

    //-- Function keys
    .F_keys         ( fkeys_s )       
);

//--------- Scandoubler and scanlines_mode toggles ----------------------------------------------------
wire btn_scandb_s;
debounce #(.counter_size  (3)) debounce_nmi1
(
   .clk_i     ( clk_24 ),
   .button_i  ( ~btn_multiface_n_i & membrane_s[8]),
   .result_o  ( btn_scandb_s )   
  );
  
wire btn_scanln_s;
debounce # (.counter_size  (3)) debounce_nmi2
(
   .clk_i     ( clk_24 ),
   .button_i  ( ~btn_multiface_n_i & membrane_s[0]),
   .result_o  ( btn_scanln_s )   
  );
  
reg [1:0]scandb_edge;
reg [1:0]scanln_edge;
reg [1:0]rotate_edge;

always @(posedge clk_24)
begin
      if (reset) 
      begin
          scandblctrl = 1'b1;
          scanlines_mode = 2'b00;
      end
      else
      begin
      
          scandb_edge = {scandb_edge[0],btn_scandb_s | fkeys_s[2]};
          if (scandb_edge == 2'b01) scandblctrl = ~scandblctrl;

          rotate_edge = {rotate_edge[0],m_tilt | membrane_s[9]};
          if (rotate_edge == 2'b01) rotatectrl = rotatectrl + 1;
          
          scanln_edge = {scanln_edge[0],btn_scanln_s | fkeys_s[7]};
          if (scanln_edge == 2'b01) scanlines_mode = scanlines_mode + 1;

      end 
end

//--------- CLUT RGB ----------------------------------------------------
dpram #(5,8)  clut 
(
    .clock_a   ( clk_12 ),
    .wren_a    ( dn_wr & (dn_addr[15:5] == 11'b10000000000)),// 8000-801f    era 6000-601F
    .address_a ( dn_addr[4:0] ),
    .data_a    ( ram_data_io[7:0] ),
    .enable_a  ( 1'b1 ),
    .cs_a      ( 1'b1 ),

    .clock_b   ( clk_12 ),
    .address_b ( idx_color_s ),
    .q_b       ( color_s ),
    .data_b    ( 8'd0 ),
    .wren_b    ( 1'b0 ),
    .enable_b  ( 1'b1 ),
    .cs_b      ( 1'b1 )
);

wire [7:0] color_s;


reg [4:0] missile_color_s =5'b11111;

always @(*)
begin

    case (core_mod)
        7'd0:    begin missile_color_s = 5'b00011; end // Galaxian
        7'd1:    begin missile_color_s = 5'b11111; end // Moon Cresta
        default: begin missile_color_s = 5'b11111; end // last palette color
    endcase

end


wire [4:0] idx1 = (W_MISSILE) ? missile_color_s : (W_STARS == 5'b00000) ? idx_color_s : W_STARS;

//--------- VGA/HDMI OUTPUT ----------------------------------------------------
    wire [8:0] video_x_s;
    wire [7:0] video_y_s;

    wire [4:0] vga_col_s;
    wire vga_blank_s, vga_out_hs_s, vga_out_vs_s;
    
    
    vga vga
    (
        .I_CLK      ( clk_12 ),
        .I_CLK_VGA  ( clk_pixel ),
        .I_COLOR    ( idx1 ),
        .I_HCNT     ( video_x_s ),
        .I_VCNT     ( video_y_s ),
        
        .I_ROTATE   ( rotatectrl ),
        
        .O_HSYNC    ( vga_hs_s ),
        .O_VSYNC    ( vga_vs_s ),
        .O_COLOR    ( vga_col_s ),
        .O_BLANK    ( vga_blank_s ),

        .O_ODD_LINE ( scanline )
    );
    

//--------- CLUT VGA ----------------------------------------------------

dpram #(5,8)  clut_vga 
(
    .clock_a   ( clk_12 ),
    .wren_a    ( dn_wr & (dn_addr[15:5] == 11'b10000000000)),// 8000-801f    
    .address_a ( dn_addr[4:0] ),
    .data_a    ( ram_data_io[7:0] ),
    .enable_a  ( 1'b1 ),
    .cs_a      ( 1'b1 ),

    .clock_b   ( clk_pixel ),
    .address_b ( vga_col_s ),
    .q_b       ( vga_color_s ),
    .data_b    ( 8'd0 ),
    .wren_b    ( 1'b0 ),
    .enable_b  ( 1'b1 ),
    .cs_b      ( 1'b1 )
);

wire [7:0] vga_color_s;

assign vga_r_s = {vga_color_s[2:0], 3'b000};
assign vga_g_s = {vga_color_s[5:3], 3'b000};
assign vga_b_s = {vga_color_s[7:6], 4'b0000};

wire    [15:0] sound_hdmi_s;
    
    wire [9:0] tdms_r_s;
    wire [9:0] tdms_g_s;
    wire [9:0] tdms_b_s;
    wire [3:0] hdmi_p_s;
    wire [3:0] hdmi_n_s;
    
 //-- HDMI
    hdmi 
    # (
        .FREQ ( 40000000 ),   //-- pixel clock frequency 
        .FS   ( 48000 ),      //-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
        .CTS  ( 40000 ),      //-- CTS = Freq(pixclk) * N / (128 * Fs)
        .N    ( 6144 )        //-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
    ) inst_dvid
    (
        .I_CLK_PIXEL    ( clk_pixel ),
        .I_R            ( {vga_r_out_s, vga_r_out_s[5:4]} ),
        .I_G            ( {vga_g_out_s, vga_g_out_s[5:4]} ),
        .I_B            ( {vga_b_out_s, vga_b_out_s[5:4]} ),
        .I_BLANK        ( vga_blank_s ),
        .I_HSYNC        ( vga_hs_s ),
        .I_VSYNC        ( vga_vs_s ),
        
        //-- PCM audio
        .I_AUDIO_ENABLE ( 1'b1 ),
        .I_AUDIO_PCM_L  ( sound_hdmi_s ),
        .I_AUDIO_PCM_R  ( sound_hdmi_s ),
        
        //-- TMDS parallel pixel synchronous outputs (serialize LSB first)
        .O_RED          ( tdms_r_s ),
        .O_GREEN        ( tdms_g_s ),
        .O_BLUE         ( tdms_b_s )
    );
    
  hdmi_out_xilinx hdmio
  (
      .clock_pixel_i  ( clk_pixel ),
      .clock_tdms_i   ( clk_dvi   ),
      .clock_tdms_n_i ( clk_dvi_n ),
      .red_i          ( tdms_r_s ),
      .green_i        ( tdms_g_s ),
      .blue_i         ( tdms_b_s ),
      .tmds_out_p     ( hdmi_p_o ),
      .tmds_out_n     ( hdmi_n_o )
  );

//   wire [15:0] pcm_s;
//   audio_pcm
//   #( .CLK_RATE (40000000))
//   audio_pcm
//   (
//       .clk         ( clk_pixel ),
//       .sample_rate ( 0 ), //48khz
//       .left_in     ( {audio, 6'b000000} ),
//       .pcm_l       ( pcm_s ),
//   );
 
   // assign sound_hdmi_s =  {1'b0, ~pcm_s[15], pcm_s[14:1]}; 
   //  assign sound_hdmi_s =  {1'b0, audio, 4'b0000}; 
      assign sound_hdmi_s =  {16'd0}; 
  

//--------- ROMS in SRAM ----------------------------------------------------
reg rom_oe, sound_oe;

assign ram_oe_n_o = 1'b0;
assign ram_we_n_o = 1'b1;
assign ram_ce_n_o = 4'b1110;

always @(*)
begin
         if (rom_oe)   rom_dout_s       <= ram_data_io[7:0];
    else explosion_data_s <= ram_data_io[7:0];
   

    
    if (dn_wr == 1'b1)          dn_data <= ram_data_io[7:0];

    if (ram_addr_o[15:0] == 16'h8020 && dn_wr == 1'b1)  core_mod <= ram_data_io[6:0]; 
end 

//PRG ROM 0000-3FFF
//GFX ROM 5000-5FFF

always @(posedge clk_24)
begin                                                             
   ram_addr_o  <= (dn_downloading) ? dn_addr[18:0] : (rom_cs_s) ? {5'b11000, rom_addr_s[13:0] } : {6'b110011,explosion_addr_s };    

    rom_oe <= rom_cs_s; //delay the clock signal one cycle to make the OE for data
end


//--------- PROM DATA PUMP --------------------------------

  reg [ 19:0] counter_addr;
  wire [18:0] dn_addr = counter_addr[19:1];
  reg         dn_wr;
  reg         dn_downloading;
  reg  [7:0]  dn_data;

  wire hard_reset = ~pll_locked;

  //--start after the FPGA power on
  always @(posedge clk_6) 
  begin

          if (hard_reset == 1)
              begin
                 pll_locked <= 1'b1;
                  dn_downloading = 1'b1;
                  counter_addr = 20'hC8000;  //0x4000 (shifted)
                  dn_wr = 1'b1;
              end
          else if (counter_addr < 20'hD0042) //0x8020 (shifted)
              begin
                  counter_addr = counter_addr + 1;
                  dn_wr = counter_addr[0]; //use the LSB bit as write clock
              end 
          else
              begin
                  dn_downloading = 1'b0;
                  dn_wr = 1'b0;
              end 
  end 
  //---------------------------------------------------------
  
  //--------------------------------------------------------
  //-- Reboot to Spectrum Next Core
  //--------------------------------------------------------
  
  reg [4:0] zxn_coreid = 5'b00001; // Spectrum Next slot 
  flashboot flashboot
  (
      .reset_i    ( hard_reset ),
      .clock_i    ( clk_24 ),
      .start_i    ( ~(btn_divmmc_n_i | btn_reset_n_i) ),
      .spiaddr_i  ( {8'h6B, zxn_coreid, 19'b0000000000000000000} )
  );

  //--------------------------------------------------------
  //-- Unused outputs
  //--------------------------------------------------------

  // -- Interal audio (speaker, not fitted)
  assign audioint_o     = 1'b0;

  //-- Spectrum Next Bus
  assign bus_addr_o     = 16'hFFF0;
  assign bus_busack_n_o = 1'b1;
  assign bus_clk35_o    = 1'b1;
  assign bus_data_io    = 8'hFF;
  assign bus_halt_n_o   = 1'b1;
  assign bus_iorq_n_o   = 1'b1;
  assign bus_m1_n_o     = 1'b1;
  assign bus_mreq_n_o   = 1'b1;
  assign bus_rd_n_o     = 1'b1;
  assign bus_rfsh_n_o   = 1'b1;
  assign bus_rst_n_io   = 1'b1;
  assign bus_wr_n_o     = 1'b1;

  //-- ESP 8266 module
  assign esp_gpio0_io   = 1'bZ;
  assign esp_gpio2_io   = 1'bZ;
  assign esp_tx_o = 1'b1;

  //-- Addtional flash pins; used at IO2 and IO3 in Quad SPI Mode
  assign flash_hold_o   = 1'b1;
  assign flash_wp_o     = 1'b1;

  assign flash_cs_n_o  = 1'b1;
  assign flash_sclk_o  = 1'b1;
  assign flash_mosi_o  = 1'b1;

  assign ear_port_i = 1'b1;

  assign i2c_scl_io = 1'bZ;
  assign i2c_sda_io = 1'bZ;

  //-- Mic Port (output, as it connects to the mic input on cassette deck)
  assign mic_port_o = 1'b0;

  //-- CS2 is for internal SD socket
  assign sd_cs1_n_o = 1'b1;

  // PI GPIO
  assign accel_io = 28'bZZZZZZZZZZZZZZZZZZZZZZZZZZZZ;

  // Vacant pins
  assign extras_io = 1'b1;

  assign csync_o = 1'b1;

  assign sd_cs0_n_o = 1'b1;
  assign sd_cs1_n_o = 1'b1;
  assign sd_sclk_o  = 1'b1;
  assign sd_mosi_o  = 1'b1;

endmodule
