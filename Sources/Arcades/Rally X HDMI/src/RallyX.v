//============================================================================
//  Arcade: New Rally-X
//
//  Original implimentation and port to MiSTer by MiSTer-X 2019
//============================================================================

//============================================================================
//
//  ZX Spectrum Next Top by Victor Trucco
//
//============================================================================

`default_nettype none

module RallyX_next(
   // Clocks
  input  wire clock_50_i,

  //SRAM (AS7C34096)
  output reg [18:0] ram_addr_o,
  inout  wire [15:0] ram_data_io,
  output wire ram_oe_n_o,
  output wire ram_we_n_o,
  output wire [3:0] ram_ce_n_o,

 // PS2
  inout wire ps2_clk_io,
  inout wire ps2_data_io,
  inout wire ps2_pin6_io,
  inout wire ps2_pin2_io,
                        
  // SD Card
  output wire sd_cs0_n_o,
  output wire sd_cs1_n_o,
  output wire sd_sclk_o,
  output wire sd_mosi_o,
  input  wire sd_miso_i,

  // Flash
  output wire flash_cs_n_o,
  output wire flash_sclk_o,
  output wire flash_mosi_o,
  input  wire flash_miso_i,
  output wire flash_wp_o,
  output wire flash_hold_o,

  // Joystick
  input  wire joyp1_i,
  input  wire joyp2_i,
  input  wire joyp3_i,
  input  wire joyp4_i,
  input  wire joyp6_i,
  output wire joyp7_o,
  input  wire joyp9_i,
  output wire joysel_o,

  // Audio
  output wire audioext_l_o,
  output wire audioext_r_o,
  output wire audioint_o,

  // K7
  input  wire ear_port_i,
  output wire mic_port_o,

  // Buttons
  input  wire btn_divmmc_n_i,
  input  wire btn_multiface_n_i,
  input  wire btn_reset_n_i,

  // Matrix keyboard
  output wire [7:0] keyb_row_o,
  input  wire [6:0] keyb_col_i,

  // Bus
  inout  wire bus_rst_n_io,
  output wire bus_clk35_o,
  output wire [15:0] bus_addr_o,
  inout  wire [7:0] bus_data_io,
  inout  wire bus_int_n_io,
  input  wire bus_nmi_n_i,
  input  wire bus_ramcs_i,
  input  wire bus_romcs_i,
  input  wire bus_wait_n_i,
  output wire bus_halt_n_o,
  output wire bus_iorq_n_o,
  output wire bus_m1_n_o,
  output wire bus_mreq_n_o,
  output wire bus_rd_n_o,
  output wire bus_wr_n_o,
  output wire bus_rfsh_n_o,
  input  wire bus_busreq_n_i,
  output wire bus_busack_n_o,
  input  wire bus_iorqula_n_i,

  // VGA
  output wire [2:0] rgb_r_o,
  output wire [2:0] rgb_g_o,
  output wire [2:0] rgb_b_o,
  output wire hsync_o,
  output wire vsync_o,
  output wire csync_o,

  // HDMI
  output wire [3:0] hdmi_p_o,
  output wire [3:0] hdmi_n_o,

  // I2C (RTC and HDMI)
  inout  wire i2c_scl_io,
  inout  wire i2c_sda_io,

  // ESP
  inout  wire esp_gpio0_io,
  inout  wire esp_gpio2_io,
  input  wire esp_rx_i,
  output wire esp_tx_o,

  // PI GPIO
  inout  wire [27:0] accel_io,

  // Vacant pins
  inout  wire extras_io
);

reg scandblctrl = 1'b1;
reg [1:0] rotatectrl  = 2'b00;

reg joysel_s = 0;
reg [5:0] joy1_pins_s;
reg [5:0] joy2_pins_s;

always @(posedge clk_24M)
begin
        joysel_s <= ~joysel_s;
        
        if (joysel_s == 0)
            joy1_pins_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
        else
           joy2_pins_s = {joyp9_i, joyp6_i, joyp4_i, joyp3_i, joyp2_i, joyp1_i};
end 

assign joysel_o = joysel_s;

assign audioext_r_o = audioext_l_o;

////////////////////   CLOCKS   ///////////////////

wire clk_dvi, clk_dvi_n, clk_pixel;
wire clk_24M,pll_locked;
wire clk_sys = clk_24M;

pll pll
(
    .CLK_IN1  (clock_50_i),
    .CLK_OUT1 (clk_24M),
    .CLK_OUT2 (clk_pixel),
    .CLK_OUT3 (clk_dvi),
    .CLK_OUT4 (clk_dvi_n),
    .LOCKED   (pll_locked)
);


///////////////////////////////////////////////////

wire        forced_scandoubler;

wire [10:0] ps2_key;
wire [15:0] joystk1, joystk2;



wire bCabinet  = 1'b0;  // (upright only)


///////////////////////////////////////////////////

wire hblank, vblank;
wire hs, vs;
wire blankn = ~(hblank | vblank);
wire [2:0]  r, g;
wire [1:0]  b;
 
wire [5:0] vga_r_s = { vga_color_s[2:0], 3'b000 }; 
wire [5:0] vga_g_s = { vga_color_s[5:3], 3'b000 }; 
wire [5:0] vga_b_s = { vga_color_s[7:6], vga_color_s[6], 3'b000 };  
    
wire vga_hs_s;  
wire vga_vs_s;  

reg [5:0] vga_r_out_s;  
reg [5:0] vga_g_out_s;  
reg [5:0] vga_b_out_s;  

wire scanline;
reg [1:0] scanlines_mode = 2'b00;

//scanlines_mode 
always @(posedge clk_pixel) 
begin

    // if no scanlines_mode or not a scanline
    if(!scanline || !scanlines_mode) 
    begin
        vga_r_out_s <= vga_r_s;
        vga_g_out_s <= vga_g_s;
        vga_b_out_s <= vga_b_s;
    end else 
    begin
        case(scanlines_mode)
            2'b01: begin // reduce 25% = 1/2 + 1/4
                vga_r_out_s <= {1'b0, vga_r_s[5:1]} + {2'b00, vga_r_s[5:2] };
                vga_g_out_s <= {1'b0, vga_g_s[5:1]} + {2'b00, vga_g_s[5:2] };
                vga_b_out_s <= {1'b0, vga_b_s[5:1]} + {2'b00, vga_b_s[5:2] };
            end

            2'b10: begin // reduce 50% = 1/2
                vga_r_out_s <= {1'b0, vga_r_s[5:1]};
                vga_g_out_s <= {1'b0, vga_g_s[5:1]};
                vga_b_out_s <= {1'b0, vga_b_s[5:1]};
            end

            2'b11: begin // reduce 75% = 1/4
                vga_r_out_s <= {2'b00, vga_r_s[5:2]};
                vga_g_out_s <= {2'b00, vga_g_s[5:2]};
                vga_b_out_s <= {2'b00, vga_b_s[5:2]};
            end
        endcase
    end
end

//--------- RGB OUTPUT ----------------------------------------------------

wire [7:0] rgb_out;

dpram #(5,8)  col_rom_rgb 
(
    .clock_a   ( clk_sys ),
    .wren_a    ( ioctl_wr & (ioctl_addr[15:5]=={8'h93,3'b000}) ),
    .address_a ( ioctl_addr ), 
    .data_a    ( ram_data_io[7:0] ),
    .enable_a  ( 1'b1 ),
    .cs_a      ( 1'b1 ),

    .clock_b   ( PCLK ),
    .address_b ( palette_addr_s ),
    .q_b       ( oPIX ),
    .data_b    ( 8'd0 ),
    .wren_b    ( 1'b0 ),
    .enable_b  ( 1'b1 ),
    .cs_b      ( 1'b1 )
);

       //-------------------------------


assign rgb_r_o = ( scandblctrl ) ? vga_r_out_s[5:3] : r;
assign rgb_g_o = ( scandblctrl ) ? vga_g_out_s[5:3] : g;
assign rgb_b_o = ( scandblctrl ) ? vga_b_out_s[5:3] : {b,b[1]};
assign hsync_o = ( scandblctrl ) ? vga_hs_s         : hs;
assign vsync_o = ( scandblctrl ) ? vga_vs_s         : vs;

wire            PCLK;
wire  [8:0] HPOS,VPOS;
HVGEN hvgen
(
    .HPOS(HPOS),.VPOS(VPOS),.PCLK(PCLK),.iRGB(oPIX),
    .oRGB({b,g,r}),.HBLK(hblank),.VBLK(vblank),.HSYN(hs),.VSYN(vs)
);


wire [15:0] AOUT;


///////////////////////////////////////////////////

wire iRST  =  ~btn_reset_n_i | ioctl_download;

//wire  [7:0] iDSW  = ~{ 2'b00, status[10:8], status[12:11], status[15] };
wire  [7:0] iDSW  = ~{ 8'd0 };
wire  [7:0] iCTR1 = ~{ btn_coin, btn_one_player, btn_up, btn_down, btn_right, btn_left, btn_fireA, 1'b0 };
wire  [7:0] iCTR2 = ~{ m_coin2, btn_two_players, m_up2, m_down2, m_right2, m_left2, m_fire2A, bCabinet };

wire  [7:0] oPIX;
wire  [7:0] oSND;

wire [15:0] cpu_a_s;
reg  [ 7:0] romdata_s;
wire [ 4:0] palette_addr_s;
wire rom_ce_s;

fpga_NRX GameCore ( 
    .RESET(iRST),.CLK24M(clk_24M),
    .HP(HPOS),.VP(VPOS),.PCLK(PCLK),
    //.POUT(oPIX),
    .SND(oSND),
    .DSW(iDSW),.CTR1(iCTR1),.CTR2(iCTR2),
    
    .ROMCL(clk_sys),.ROMAD(ioctl_addr),.ROMDT(ram_data_io[7:0]),.ROMEN(ioctl_wr),

    .cpu_a_o  ( cpu_a_s   ),
    .rom_ce_o ( rom_ce_s  ),
    .romdata  ( romdata_s ),

    .palette_addr_o ( palette_addr_s )

);

assign AOUT = {oSND,8'h0};

dac #(
    .C_bits(16))
dac(
    .clk_i(clk_sys),
    .res_n_i(1),
    .dac_i(AOUT),
    .dac_o(audioext_l_o)
    );


//-----------------------

reg [3:0] clk_kbd;

always @(posedge clk_sys)
begin
    clk_kbd <= clk_kbd + 1'd1;
end


wire m_up, m_down, m_left, m_right, m_fireA, m_fireB, m_fireC, m_fireD, m_fireE, m_fireF, m_fireG;
wire m_up2, m_down2, m_left2, m_right2, m_fire2A, m_fire2B, m_fire2C, m_fire2D, m_fire2E, m_fire2F, m_fire2G;
wire m_tilt, m_coin1, m_coin2, m_coin3, m_coin4, m_one_player, m_two_players, m_three_players, m_four_players;

wire m_right4, m_left4, m_down4, m_up4, m_right3, m_left3, m_down3, m_up3;

wire btn_one_player  = m_one_player  | membrane_s[7];
wire btn_two_players = m_two_players | membrane_s[8];
wire btn_coin        = m_coin1       | membrane_s[6];

wire btn_up    = m_up    | membrane_s[0];
wire btn_down  = m_down  | membrane_s[1];
wire btn_left  = m_left  | membrane_s[2];
wire btn_right = m_right | membrane_s[3];
wire btn_fireA = m_fireA | membrane_s[4] | membrane_s[5];

wire kbd_intr;
wire [7:0] kbd_scancode;
wire [7:0] keys_s;
wire [9:0] membrane_s;

//get scancode from keyboard
io_ps2_keyboard keyboard 
 (
  .clk       ( clk_sys ),
  .kbd_clk   ( ps2_clk_io ),
  .kbd_dat   ( ps2_data_io ),
  .interrupt ( kbd_intr ),
  .scancode  ( kbd_scancode )
);

membrane_joystick membrane_joystick 
(

  .clock            ( clk_sys ),
  .reset            ( iRST ),

  .membrane_joy_o   ( membrane_s ), //-- P2, P1, COIN, Fire 2, Fire 1, R, L, D, U
    
  .keyb_row_o       ( keyb_row_o ),   
  .i_membrane_cols  ( keyb_col_i )

);
    
wire [15:0] joy1_s;
wire [15:0] joy2_s;
wire [8:0]  controls_s;
wire [12:1] fkeys_s;

//translate scancode to joystick
kbd_joystick k_joystick
(
    .clk            ( clk_sys ),
    .kbdint         ( kbd_intr ),
    .kbdscancode    ( kbd_scancode ), 

    .joystick_0     ({ joy1_pins_s[5:4], joy1_pins_s[0], joy1_pins_s[1], joy1_pins_s[2], joy1_pins_s[3] }),
    .joystick_1     ({ joy2_pins_s[5:4], joy2_pins_s[0], joy2_pins_s[1], joy2_pins_s[2], joy2_pins_s[3] }),

    //-- joystick_0 and joystick_1 should be swapped
    .joyswap        ( 0 ),

    //-- player1 and player2 should get both joystick_0 and joystick_1
    .oneplayer      ( 1 ),

    //-- tilt, coin4-1, start4-1
    .controls       ( {m_tilt, m_coin4, m_coin3, m_coin2, m_coin1, m_four_players, m_three_players, m_two_players, m_one_player} ),

    //-- fire12-1, up, down, left, right

    .player1        ( {m_fireG,  m_fireF, m_fireE, m_fireD, m_fireC, m_fireB, m_fireA, m_up, m_down, m_left, m_right} ),
    .player2        ( {m_fire2G, m_fire2F, m_fire2E, m_fire2D, m_fire2C, m_fire2B, m_fire2A, m_up2, m_down2, m_left2, m_right2} ),

    //-- keys to the OSD
    .osd_o          (  ),
    .osd_enable     ( 1'b0 ),

    //-- sega joystick
    .sega_clk       ( hs ),
    .sega_strobe    ( joyp7_o ),

    //-- Function keys
    .F_keys         ( fkeys_s )       
);

//--------- Scandoubler and scanlines_mode toggles ----------------------------------------------------
wire btn_scandb_s;
debounce #(.counter_size  (3)) debounce_nmi1
(
   .clk_i     ( clk_sys ),
   .button_i  ( ~btn_multiface_n_i & membrane_s[8]),
   .result_o  ( btn_scandb_s )   
  );
  
wire btn_scanln_s;
debounce # (.counter_size  (3)) debounce_nmi2
(
   .clk_i     ( clk_sys ),
   .button_i  ( ~btn_multiface_n_i & membrane_s[0]),
   .result_o  ( btn_scanln_s )   
  );
  
reg [1:0]scandb_edge;
reg [1:0]scanln_edge;
reg [1:0]rotate_edge;

always @(posedge clk_sys)
begin
      if (iRST) 
      begin
          scandblctrl = 1'b1;
          scanlines_mode = 2'b00;
      end
      else
      begin
      
          scandb_edge = {scandb_edge[0],btn_scandb_s | fkeys_s[2]};
          if (scandb_edge == 2'b01) scandblctrl = ~scandblctrl;

          rotate_edge = {rotate_edge[0],m_tilt | membrane_s[9]};
          if (rotate_edge == 2'b01) rotatectrl = rotatectrl + 1;
          
          scanln_edge = {scanln_edge[0],btn_scanln_s | fkeys_s[7]};
          if (scanln_edge == 2'b01) scanlines_mode = scanlines_mode + 1;

      end 
end


//--------- VGA/HDMI OUTPUT ----------------------------------------------------


    wire [4:0] vga_col_s;
    wire vga_blank_s, vga_out_hs_s, vga_out_vs_s;
    
    
    vga vga
    (
        .I_CLK      ( PCLK ),
        .I_CLK_VGA  ( clk_pixel ),
        .I_COLOR    ( palette_addr_s[3:0] ),
        .I_HCNT     ( HPOS ),
        .I_VCNT     ( VPOS ),
        
        .I_ROTATE   ( 2'b00 ), //no rotation
        
        .O_HSYNC    ( vga_hs_s ),
        .O_VSYNC    ( vga_vs_s ),
        .O_COLOR    ( vga_col_s ),
        .O_BLANK    ( vga_blank_s ),

        .O_ODD_LINE ( scanline )
    );
    

//--------- CLUT VGA ----------------------------------------------------

dpram #(5,8)  col_rom_vga 
(
    .clock_a   ( clk_sys ),
    .wren_a    ( ioctl_wr & (ioctl_addr[15:5]=={8'h93,3'b000}) ),
    .address_a ( ioctl_addr ), 
    .data_a    ( ram_data_io[7:0] ),
    .enable_a  ( 1'b1 ),
    .cs_a      ( 1'b1 ),

    .clock_b   ( clk_pixel ),
    .address_b ( vga_col_s ),
    .q_b       ( vga_color_s ),
    .data_b    ( 8'd0 ),
    .wren_b    ( 1'b0 ),
    .enable_b  ( 1'b1 ),
    .cs_b      ( 1'b1 )
);

wire [7:0] vga_color_s;



    
 //---- HDMI -----------------------------------------

    wire    [15:0] sound_hdmi_s;
    
    wire [9:0] tdms_r_s;
    wire [9:0] tdms_g_s;
    wire [9:0] tdms_b_s;
    wire [3:0] hdmi_p_s;
    wire [3:0] hdmi_n_s;

    hdmi 
    # (
        .FREQ ( 27000000 ),   //-- pixel clock frequency 
        .FS   ( 48000 ),      //-- audio sample rate - should be 32000, 41000 or 48000 = 48KHz
        .CTS  ( 27000 ),      //-- CTS = Freq(pixclk) * N / (128 * Fs)
        .N    ( 6144 )        //-- N = 128 * Fs /1000,  128 * Fs /1500 <= N <= 128 * Fs /300 (Check HDMI spec 7.2 for details)
    ) inst_dvid
    (
        .I_CLK_PIXEL    ( clk_pixel ),
        .I_R            ( {vga_r_out_s, vga_r_out_s[5:4]} ),
        .I_G            ( {vga_g_out_s, vga_g_out_s[5:4]} ),
        .I_B            ( {vga_b_out_s, vga_b_out_s[5:4]} ),
        .I_BLANK        ( vga_blank_s ),
        .I_HSYNC        ( vga_hs_s ),
        .I_VSYNC        ( vga_vs_s ),
        
        //-- PCM audio
        .I_AUDIO_ENABLE ( 1'b1 ),
        .I_AUDIO_PCM_L  ( sound_hdmi_s ),
        .I_AUDIO_PCM_R  ( sound_hdmi_s ),
        
        //-- TMDS parallel pixel synchronous outputs (serialize LSB first)
        .O_RED          ( tdms_r_s ),
        .O_GREEN        ( tdms_g_s ),
        .O_BLUE         ( tdms_b_s )
    );
 
    hdmi_out_xilinx hdmio
    (
      .clock_pixel_i  ( clk_pixel ),
      .clock_tdms_i   ( clk_dvi   ),
      .clock_tdms_n_i ( clk_dvi_n ),
      .red_i          ( tdms_r_s ),
      .green_i        ( tdms_g_s ),
      .blue_i         ( tdms_b_s ),
      .tmds_out_p     ( hdmi_p_o ),
      .tmds_out_n     ( hdmi_n_o )
    );

      assign sound_hdmi_s =  AOUT; //{16'd0}; 
    



//--------- ROMS in SRAM ----------------------------------------------------

// 0000-3FFF    CPU instruction
// 4000-7FFF    "BANG!" PCM data
// 8000-8FFF    Background/Object pattern
// 9000-90FF    Rader dot pattern
// 9100-91FF    Sound wave
// 9200-92FF    Lookup table
// 9300-931F    Palette

reg rom_oe;

assign ram_oe_n_o   = 1'b0; 
assign ram_ce_n_o = 4'b1110;    
assign ram_we_n_o   = 1'b1; 

always @(posedge clk_sys)
begin                                                           
    ram_addr_o  <= (ioctl_wr) ? ioctl_addr[18:0] : (rom_ce_s) ? {6'b11000, cpu_a_s[13:0] } : {6'b11000, cpu_a_s[13:0] };    

    rom_oe <= rom_ce_s; //delay the clock signal one cycle to get the SRAM data
end

always @(*)
begin

    if (rom_oe) romdata_s <= ram_data_io[7:0];
   
end 

  //--------- PROM DATA PUMP --------------------------------

  reg [ 19:0] counter_addr;
  wire [18:0] ioctl_addr = counter_addr[19:1];
  reg         ioctl_wr;
  reg         ioctl_download;

reg power_on = 1'b0;

  wire hard_reset = ~power_on;

reg [3:0] clk_cnt;

always @(posedge clk_sys)
begin
    clk_cnt <= clk_cnt + 1'd1;
end

  //--start after the FPGA power on
  always @(posedge clk_cnt[1]) //~6mhz 
  begin

          if (hard_reset == 1)
              begin
                  power_on <= 1'b1;
                  ioctl_download = 1'b1;
                  counter_addr = 20'hc8000; //0x4000 (shifted)
                  ioctl_wr = 1'b1;
              end
          else if (counter_addr < 20'hd4000) //0xa000 (shifted)
              begin
                  counter_addr = counter_addr + 1;
                  ioctl_wr = counter_addr[0]; //use the LSB bit as write clock
              end 
          else
              begin
                  ioctl_download = 1'b0;
                  ioctl_wr = 1'b0;
              end 
  end 
  //---------------------------------------------------------

  //--------------------------------------------------------
  //-- Reboot to Spectrum Next Core
  //--------------------------------------------------------
  
  reg [4:0] zxn_coreid = 5'b00001; // Spectrum Next slot 
  flashboot flashboot
  (
      .reset_i        ( hard_reset ),
      .clock_i        ( clk_sys ),
      .start_i        ( ~(btn_divmmc_n_i | btn_reset_n_i) ),
      .spiaddr_i  ( {8'h6B, zxn_coreid, 19'b0000000000000000000} )
  );

  //--------------------------------------------------------
  //-- Unused outputs
  //--------------------------------------------------------

  // -- Interal audio (speaker, not fitted)
  assign audioint_o     = 1'b0;

  //-- Spectrum Next Bus
  assign bus_addr_o     = 16'hFFFF;
  assign bus_busack_n_o = 1'b1;
  assign bus_clk35_o    = 1'b1;
  assign bus_data_io    = 8'hFF;
  assign bus_halt_n_o   = 1'b1;
  assign bus_iorq_n_o   = 1'b1;
  assign bus_m1_n_o     = 1'b1;
  assign bus_mreq_n_o   = 1'b1;
  assign bus_rd_n_o     = 1'b1;
  assign bus_rfsh_n_o   = 1'b1;
  assign bus_rst_n_io   = 1'b1;
  assign bus_wr_n_o     = 1'b1;

  //-- ESP 8266 module
  assign esp_gpio0_io   = 1'bZ;
  assign esp_gpio2_io   = 1'bZ;
  assign esp_tx_o = 1'b1;

  //-- Addtional flash pins; used at IO2 and IO3 in Quad SPI Mode
  assign flash_hold_o   = 1'b1;
  assign flash_wp_o     = 1'b1;

  assign flash_cs_n_o  = 1'b1;
  assign flash_sclk_o  = 1'b1;
  assign flash_mosi_o  = 1'b1;

  assign ear_port_i = 1'b1;

  assign i2c_scl_io = 1'bZ;
  assign i2c_sda_io = 1'bZ;

  //-- Mic Port (output, as it connects to the mic input on cassette deck)
  assign mic_port_o = 1'b0;

  //-- CS2 is for internal SD socket
  assign sd_cs1_n_o = 1'b1;

  // PI GPIO
  assign accel_io = 28'bZZZZZZZZZZZZZZZZZZZZZZZZZZZZ;

  // Vacant pins
  assign extras_io = 1'b1;

  assign csync_o = 1'b1;

  assign sd_cs0_n_o = 1'b1;
  assign sd_cs1_n_o = 1'b1;
  assign sd_sclk_o  = 1'b1;
  assign sd_mosi_o  = 1'b1;


endmodule


module HVGEN
(
    output wire [8:0]   HPOS,
    output wire [8:0]   VPOS,
    input  wire         PCLK,
    input  wire [7:0]   iRGB,

    output reg  [7:0]   oRGB,
    output reg          HBLK = 1,
    output reg          VBLK = 1,
    output reg          HSYN = 1,
    output reg          VSYN = 1
);

reg [8:0] hcnt = 0;
reg [8:0] vcnt = 0;

assign HPOS = hcnt;
assign VPOS = vcnt;

always @(posedge PCLK) begin
    case (hcnt)
        288: begin HBLK <= 1; hcnt <= hcnt+1; end
        311: begin HSYN <= 0; hcnt <= hcnt+1; end
        342: begin HSYN <= 1; hcnt <= 471;    end
        511: begin HBLK <= 0; hcnt <= 0;
            case (vcnt)
                223: begin VBLK <= 1; vcnt <= vcnt+1; end
                226: begin VSYN <= 0; vcnt <= vcnt+1; end
                233: begin VSYN <= 1; vcnt <= 483;    end
                511: begin VBLK <= 0; vcnt <= 0;          end
                default: vcnt <= vcnt+1;
            endcase
        end
        default: hcnt <= hcnt+1;
    endcase
    oRGB <= (HBLK|VBLK) ? 12'h0 : iRGB;
end

endmodule

