You can help support these cores becoming a patron
https://www.patreon.com/vtrucco

---------------------------------------------------------------------------------
-- 
-- Midway 8080 Hardware
-- Audio based on work by Paul Walsh.
-- Audio and scan converter by MikeJ.
-- MiST port by Gehstock 
-- 
---------------------------------------------------------------------------------
-- 
-- ZX Spectrum Next port by Victor Trucco - 2020
-- 
---------------------------------------------------------------------------------
-- 
-- PS/2 Keyboard:
--
--   5           : Coin
--   1           : Start 1P
--   2           : Start 2P
--   SPACE       : Fire
--   RIGHT/LEFT  : Movement
--   F2          : VGA/RGB Toggle
--   C           : Patrons screen
--
--
-- Membrane Keyboard:
--
--   3           : Coin
--   1           : Start 1P
--   2           : Start 2P
--   SPACE       : Fire
--   RIGHT/LEFT  : Movement
--   O/P         : Movement
--   NMI + 2     : VGA/RGB Toggle
--   C           : Patrons screen
-- 
--
-- Joystick:
--
--   Button      : Fire
--   RIGHT/LEFT  : Movement
--
---------------------------------------------------------------------------------

You can help support these cores becoming a patron
https://www.patreon.com/vtrucco