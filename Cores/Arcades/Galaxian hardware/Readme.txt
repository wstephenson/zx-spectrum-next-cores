GALAXIAN ARCADE HARDWARE

Ported to ZX Spectrum Next by Victor Trucco 2020
If you like this core, please support it at https://www.patreon.com/vtrucco

DISCLAIMERS:
------------
THIS CORE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE CORE OR THE USE OR OTHER DEALINGS IN THE CORE.

THIS CORE CANNOT OPERATE WITHOUT SOFTWARE (ROMS). IT IS ILLEGAL TO DOWNLOAD THE REQUIRED SOFTWARE (ROMS) IF
YOU DO NOT LEGALLY OWN IT! USE AT YOUR OWN RISK. THE INSTRUCTIONS ON PREPARING THE SOFTWARE FOR USE WITH THIS
CORE DO NOT IMPLY SANCTIONING OF ILLEGALLY OBTAINING THE SOFTWARE AND IT IS ONLY PROVIDED TO HELP LEGAL OWNERS
OF THE SOFTWARE PREPARE IT FOR USE WITH THIS CORE.

Introduction
------------
This core synthesizes the hardware of the Galaxian Arcade machine and can be used with original Galaxian as well as other games that use the same platform. The complete supported game list can be found below.


Features on the ZX Spectrum Next
--------------------------------
    - HDMI (no sound)
    - VGA and RGB  
    - Screen Rotation by hardware
    - Membrane Keyboard
    - 2 joysticks
    - PS/2 keyboard suport for dev boards

Usage instructions
------------------
On Membrane keyboard
    Movements: Q,A,O,P or cursor keys 
    Action button: Space
    Coin: 3
    Player 1 Start: 1
    Player 2 Start: 2
    Rotate screen: R (cycles 90o, 180o, 270o, off)
    Scanlines toggle: NMI button + 7 (cycles 25%, 50%, 75%, 0%)
    RGB toggle: NMI button + 2
    Reset: Reset button

On PS/2 keyboard
    Movements: cursor keys 
    Action button: Space
    Coin: 5
    Player 1 Start: 1
    Player 2 Start: 2
    Rotate screen: R (cycles 90o, 180o, 270o, off)
    Scanlines toggle: F7 (cycles 25%, 50%, 75%, 0%)
    RGB toggle: F2

You don´t need to power cycle to change the game or return to the standard ZX Spectrum Next core. 
Pressing the Reset Button + Drive button will get you to the ZX Spectrum Next boot screen where you 
can press 'C' to select the core and upon boot a new game (if more than one are installed)

GAMES SUPPORTED
---------------
Below is the list of the games supported and their usual packaging in order to find the correct
version supported by the core

     - Azurian Attack    ( azurian.zip )
     - Black Hole        ( blkhole.zip )
     - Catacomb          ( catacomb.zip )
     - Chewing Gum       ( chewing.zip )
     - Devil Fish        ( devilfsg.zip )
     - Galaxian          ( galaxian.zip )
     - King and Balloons ( kingball.zip )
     - Lucky Today       ( luctoday.zip )
     - Moon Cresta       ( mooncrgx.zip )
     - Mr. Do Nightmare  
     - Omega             ( omegab.zip )
     - Orbitron          ( orbitron.zip )
     - Pisces            ( pisces.zip )
     - Triple Draw Poker ( tdpgal.zip )
     - UniWar S          ( uniwars.zip )
     - Victory           ( victoryc.zip and victorycb.zip )
     - War of the Bugs   ( warofbug.zip )

How to prepare the software (ROMS) for use
------------------------------------------
You need the original "non-merged" arcade roms to play with this core. 
Place your ROMs zipped (using the archive names listed above) inside the /ROMs folder and use the ".bat" files included to generate each game. 
(Check the command screen for eventual error messages)

NOTE
=======================================================================================================================
If you run a batch file without having the corresponding zip file, a ROM file *will be* generated but will be unusable.
=======================================================================================================================

Once you are done generating the ROMS place your Next SD card in your PC's reader and copy the files inside the folder named "copy to your SD machines folder" to your SD "/machines/" folder. The core is included.


Flashing and booting the core (Requires Firmware 1.29B)
-------------------------------------------------------
Power up your Next and hold the "C" key for the extra cores menu. 
Select a free slot to flash the core and press SPACE. The core will be flashed. Once Flashing is complete,
the core can be started with 'ENTER' and then you will be asked to select a game from the ones you converted.
If the game doesn't run, go back and repeat the conversion process after making sure you have the appropriate
ROMS zipped in the correct location with the correct name (see NOTE above)
At the time of writing 17 games are supported, while only needing ONE slot of the cores flash memory.

-----------------------------

Thanks to Phoebus Dokos for the beta testing and this "readme.txt" file revision.

If you like this core, please support it at https://www.patreon.com/vtrucco

CHANGELOG

001 - 23/05/2020 - Initial Release